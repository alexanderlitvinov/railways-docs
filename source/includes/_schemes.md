# schemes

## saveScheme

Добавление схемы в базу данных

> Request

```http
POST /_api/rzd/saveScheme HTTP/1.1
Host: www.onetwotrip.com
Content-Type: application/json
Cache-Control: no-cache

{
    "id": 401,
    "clientName": "ios",
    "type": "internal",
    "scheme": {}
}
```

> Response

```http
HTTP/1.1 200 OK
Server: nginx/1.11.3
Date: Wed, 25 Jan 2017 15:50:31 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Set-Cookie: _sst=1485359431257|1485359431257|eda96921495a2366dcc59235d200fd2c; path=/; Secure
Set-Cookie: sid=WLF6bR4c5VMlUetWLc9OYjom; expires=Thu, 26-Jan-2017 15:50:31 GMT; path=/; domain=.onetwotrip.com; Secure
Content-Encoding: gzip
Set-Cookie: ENVID=production-a|WIjFx; path=/; Secure

{
  "success": true,
  "result": "OK"
}

```

### HTTP Request

`POST $API_HOST/_api/rzd/saveScheme`

### Post body

Параметр | Описание
--------- | -----------
id | ID схемы с ржд
clientName | Название клиента для которого предназначается схема (ios/android/web)
type | Бывает два вида схем - overview и internal
scheme | Объект схемы

## schemeIds

Получение популярности схем.
 
> Request

```http
GET /_api/rzd/schemeIds HTTP/1.1
Host: www.onetwotrip.com
Content-Type: application/json
Cache-Control: no-cache
```

> Response

```http
HTTP/1.1 200 OK
Server: nginx/1.11.3
Date: Wed, 25 Jan 2017 15:59:04 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Set-Cookie: _sst=1485359280365|1485359944829|17c7d04895060ead0fda4cdb56dcc362; path=/; Secure
Set-Cookie: sid=WLF6bR4c5VMlUetWLc9OYjom; expires=Thu, 26-Jan-2017 15:59:04 GMT; path=/; domain=.onetwotrip.com; Secure
Content-Encoding: gzip
Set-Cookie: ENVID=production-b|WIjLS; path=/; Secure
Cache-control: private

{
  "seccuss": true,
  "result": {
    "2": "9354",
    "410": "2"
  }
}
```

<aside class="notice">
    В ответе указаны даже те id, для которых у нас нет схем.
</aside>
 

### HTTP Request

`GET $API_HOST/_api/rzd/schemeIds`

### Response description
Поле | Описание
--------- | -----------
Ключ | ID Схемы
Значение | Количество запросов

## trainSchemes

Получение схем всех вагонов поезда

> Request

```http
GET /ru/_api/rzd/trainSchemes?from=2000000&amp;to=2004000&amp;train=016%D0%90&amp;date=21022017&amp;clientName=ios HTTP/1.1
Host: www.onetwotrip.com
Cache-Control: no-cache
```

> Response

```http
HTTP/1.1 200 OK
Server: nginx/1.11.3
Date: Thu, 26 Jan 2017 11:27:55 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Set-Cookie: _sst=1485427843634|1485430058232|209630b8fb685813e6dc475f0c423123; path=/; Secure
Set-Cookie: sid=07hnb4ggjbb2pQhq8Wu7ko3d; expires=Fri, 27-Jan-2017 11:27:38 GMT; path=/; domain=.onetwotrip.com; Secure
Content-Encoding: gzip
Set-Cookie: ENVID=production-a|WIndP; path=/; Secure
Cache-control: private

{
  "success": true,
  "result": {
    "cars": [
      {
        "number": 5,
        "classService": "1Э",
        "scheme": 1686
      }
    ],
    "schemes": [
      {
        "id": 24,
        "ios": {}
      }
    ]
  }
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/trainSchemes?from=2000000&to=2004000&train=016%D0%90&date=24012016&clientName=ios`

### Params

Параметр | Описание
--------- | -----------
from | Код станции отправления
to | Код станции назначения
train | Номер поезда
date | Дата отправления в формате DDMMYYYY
clientName | ios/android/web, по-умолчанию для internal = rzd
type | internal/overview по-умолчанию overview
force | Не учитывать кеш

<aside class="notice">
    Для схем типа internal при отсутствии схемы клиента возвращаются схемы с rzd
</aside>

### Response description

Поле | Описание
---- | --------
cars[].number | Номер вагона
cars[].classService | Класс обслуживания
cars[].scheme | ID схемы
schemes[].id | ID схемы
schemes[].{clientName} | Объект схемы

## carScheme

Получение схемы вагона по ID

> Request

```http
GET /_api/rzd/carScheme?id=410&amp;clientName=web HTTP/1.1
Host: www.onetwotrip.com
Cache-Control: no-cache
```

> Response

```http
HTTP/1.1 200 OK
Server: nginx/1.11.3
Date: Thu, 26 Jan 2017 11:40:13 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Set-Cookie: _sst=1485430813362|1485430813362|633fdd3b48e05cbb0fb9368acd331f24; path=/; Secure
Set-Cookie: sid=ay77uJddmoJHGXDIazUa9xSY; expires=Fri, 27-Jan-2017 11:40:13 GMT; path=/; domain=.onetwotrip.com; Secure
Content-Encoding: gzip
Set-Cookie: ENVID=production-b|WIngI; path=/; Secure
Cache-control: private

{
  "success": true,
  "result": {
    "id": 410,
    "web": {}
  }
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/carScheme?id=401&clientName=ios`

### Params

Параметр | Описание
--------- | -----------
id | ID Схемы
type | Тип схемы internal/overview, по-умолчанию internal
clientName | ios/android/web, по-умолчанию rzd

<aside class="notice">
    Для схемы типа internal при отсутствии схемы клиента возвращается rzd
</aside>

### Response description

Поле | Описание
--------- | -----------
id | ID схемы
{ios/web/android/rzd} | Объект съемы для клиента указанного в ключе

## scheme

Получение схем вагонов с сайта ржд

<aside class="warning">
    DEPRECATED - Вместо этого метода следует использовать trainSchemes или carScheme
</aside>

> Request

```http
GET /_api/rzd/searchManager?type=scheme&amp;from=2000000&amp;to=2004000&amp;train=016А&amp;date=21022017&amp;force=true HTTP/1.1
Host: www.onetwotrip.com
Cache-Control: no-cache
```

> Response

```http
HTTP/1.1 200 OK
Server: nginx/1.11.3
Date: Thu, 26 Jan 2017 12:38:03 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Set-Cookie: _sst=1485430869067|1485434282842|0248f27af080df72f4da5112b9ed138a; path=/; Secure
Set-Cookie: sid=/IzBZd92cz2VsXSMGRdNddwZ; expires=Fri, 27-Jan-2017 12:38:02 GMT; path=/; domain=.onetwotrip.com; Secure
Content-Encoding: gzip
Set-Cookie: ENVID=production-a|WIntr; path=/; Secure
Cache-control: private

{
  "success": true,
  "result": {
    "from": "2000000",
    "to": "2004000",
    "date": "21022017",
    "cars": [
      {
        "number": 5,
        "classService": "2Э",
        "scheme": 24
      }
    ],
    "schemes": [
      {
        "id": 1686,
        "len": 5,
        "cells": [
          {
            "type": "dn",
            "number": 1
          },
          {
            "type": "dn",
            "number": 3
          },
          {
            "type": "XX"
          },
          {
            "type": "dn",
            "number": 5
          },
          {
            "type": "dn",
            "number": 7
          },
          {
            "type": "XX"
          },
          {
            "type": "XX"
          },
          {
            "type": "XX"
          },
          {
            "type": "XX"
          },
          {
            "type": "XX"
          }
        ]
      }
    ]
  }
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/searchManager?type=scheme&from=2000000&to=2004000&train=016А&date=24012015&force=true`

### Params

Параметр | Описание
--------- | -----------
type | **scheme** Обязательный параметр-ключ
from | Код станции отправления
to | Код станции прибытия
date | Дата отправления в формате DDMMYYYY
train | Номер поезда (использовать trainNumber2 если есть)
force | Не использовать кешированые схемы если true

### Response description

Поле | Описание
--------- | -----------
from | Код станции отправления
to | Код станции прибытия
date | Дата отправления в формате DDMMYYYY
cars[].number | Номер вагона
cars[].classService | Класс обслуживания
cars[].scheme | ID схемы
schemes[].id | ID схемы
schemes[][other] | Остальные поля ржд схем
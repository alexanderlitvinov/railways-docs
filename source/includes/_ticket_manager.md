# ticketManager

## getTicket

Получение информации о билете.

> Request

```http
GET /_api/rzd/ticketManager?type=getTicket&reservationId=144db49d-3cf0-4606-ac0d-c121e2e455bf&ticketId=242562755&raw=true HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: www.onetwotrip.com
```

> Response

```http
HTTP/1.1 200 OK
Cache-control: private
Content-Encoding: gzip
Content-Type: application/json;charset=utf-8
Date: Thu, 19 Jan 2017 13:21:43 GMT
Server: nginx/1.11.3
Set-Cookie: _sst=1484832102012|1484832102012|5ad1f587daa0ee354316b419adca60a7; path=/; Secure
Set-Cookie: sid=hj5mNJCxgFGL0V3MBv6/55lq; expires=Fri, 20-Jan-2017 13:21:42 GMT; path=/; domain=.onetwotrip.com; Secure
Set-Cookie: ENVID=production-a|WIC9a; path=/; Secure
Transfer-Encoding: chunked

{
  "blank": {
    "raw": "... html string ...",
    "barcode": {
      "content": "... base64 string ...",
      "meta": {
        "contentType": "image/png"
      }
    },
    "code2d": {
      "content": "... base64 string ...",
      "meta": {
        "contentType": "image/gif"
      }
    }
  },
  "parsed": {
    "finInfo": {
      "type": "Детский / Child",
      "price": "68.30  (в т.ч НДС 10.41)",
      "currency": "RUR"
    },
    "trainInfo": {
      "train": "305ЯА   -",
      "car": "12 Общий",
      "place": "052"
    },
    "pax": {
      "name": "ИВАНОВ И И",
      "document": "СР *****3123   05.05.2008 / RUS / M"
    },
    "bookingDate": null,
    "orderNumber": "78941317111810",
    "departureDate": {
      "date": "23.12",
      "time": "07:26",
      "year": "2014"
    },
    "arrivalDate": {
      "date": "23.12",
      "time": "07:52"
    },
    "directions": [
      "СОСНОГОРСК - > УХТА",
      "SOSNOGORSK - > UHTA"
    ]
  },
  "ottOrderNumber": "E35660789",
  "rzdTicketNumber": "78941317111810"
}
```

### HTTP Request

`GET https://www.onetwotrip.com/_api/rzd/ticketManager?type=getTicket&reservationId={String}&ticketId={Number}`

### Required query params

Параметр | Описание
--------- | -----------
type | Значение `getTicket`
reservationId | ID жд брони
ticketId | Номер билета

### Query params

Параметр | Default | Описание
--------- | ----------- | -----------
screenshot | False | Если true - старое поведение, то есть скриншот страницы РЖД
force | False | Eсли true и screenshot=true - обновляет кеш маршрутки
barcode | False | Если true - возвращает картинку баркода
code2d | False | Если true - возвращает картинку посадочного купона
raw | False | Если true - возвращает огромную и сырую HTML в свойстве blank ответа

<aside class="success">
Чтобы обновить кеш маршрутки - force=true&schreenshot=true
</aside>

## getRefund

Расчет суммы к возврату при отмене билета.

> Request

```http
GET /_api/rzd/ticketManager?type=getRefund&reservationId=144db49d-3cf0-4606-ac0d-c121e2e455bf&ticketId=242562755 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: www.onetwotrip.com
```

> Response

```http
HTTP/1.1 200 OK
Cache-control: private
Content-Encoding: gzip
Content-Type: application/json;charset=utf-8
Date: Thu, 19 Jan 2017 13:25:12 GMT
Server: nginx/1.11.3
Set-Cookie: _sst=1484832301116|1484832301116|ad7c68c14b192679a687d261bb6fa499; path=/; Secure
Set-Cookie: sid=ZicFow59b6t4MxJYB28v+2mG; expires=Fri, 20-Jan-2017 13:25:01 GMT; path=/; domain=.onetwotrip.com; Secure
Set-Cookie: ENVID=production-a|WIC+O; path=/; Secure
Transfer-Encoding: chunked

{
	"success": true,
	"result": 30,
	"sum": 30,
	"refundAmount": 0,
	"policyRefundAmount": 0,
	"bonusRefundAmount": 30
}
```

> Error response example

```http
HTTP/1.1 200 OK
Cache-control: private
Content-Encoding: gzip
Content-Type: application/json;charset=utf-8
Date: Thu, 19 Jan 2017 13:25:12 GMT
Server: nginx/1.11.3
Set-Cookie: _sst=1484832301116|1484832301116|ad7c68c14b192679a687d261bb6fa499; path=/; Secure
Set-Cookie: sid=ZicFow59b6t4MxJYB28v+2mG; expires=Fri, 20-Jan-2017 13:25:01 GMT; path=/; domain=.onetwotrip.com; Secure
Set-Cookie: ENVID=production-a|WIC+O; path=/; Secure
Transfer-Encoding: chunked

{
    "error": {
        "code": 550,
        "details": {
            "msg": "already refund"
        },
        "message": {
            "ru": "Данный билет уже отменен."
        },
        "name": "RailwaysError"
    },
    "success": false
}
```

### HTTP Request

`GET https://www.onetwotrip.com/_api/rzd/ticketManager?type=getRefund&reservationId={String}&ticketId={Number}`

### Required query params

Параметр | Описание
--------- | -----------
type | Значение `getRefund`
reservationId | ID жд брони
ticketId | Номер билета

В ответ приходит общая сумма к возврату, количество бонусов/денег на возврат.

 * sum / refundAmount - общая сумма денег за возврат
 * bonusRefundAmount - общая сумма бонусов за возврат
 * policyRefundAmount - сколько денег из refundAmount возвращаются за страховку (policyRefundAmount < refundAmount)

## cancel

Отмена жд билета.

> Request

```http
GET /_api/rzd/ticketManager?type=cancel&reservationId=144db49d-3cf0-4606-ac0d-c121e2e455bf&ticketId=242562755 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: www.onetwotrip.com
```


> Response

```json
{
	"status": "OK"
}
```

### HTTP Request

`GET https://www.onetwotrip.com/_api/rzd/ticketManager?type=cancel&reservationId={String}&ticketId={Number}`

### Required query params

Параметр | Описание
--------- | -----------
type | Значение `getRefund`
reservationId | ID жд брони
ticketId | Номер билета

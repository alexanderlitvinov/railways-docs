# booking

## bookStart

> Request

```http
POST /rzd/bookManager HTTP/1.1
Accept: application/json
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 1299
Content-Type: application/json
Host: onetwotrip.com

{
    "buyerInfo": {
        "mail": "ruslan.ismagilov@onetwotrip.com",
        "phone": "79152792418"
    },
    "carNum": 2,
    "carType": "2",
    "device_id": "1HljtHsWYR7WItKujA9PL3go",
    "device_name": "web",
    "lat": 55.7788674,
    "long": 37.6235121,
    "paxes": [
        {
            "birthDate": "11.11.2010",
            "birthPlace": "Москва",
            "country": "RU",
            "docNumber": "IIБЦ111222",
            "docType": 6,
            "name": {
                "first": "лучший",
                "last": "самый",
                "mid": "человек"
            },
            "seats": [
                70
            ],
            "sex": 1
        }
    ],
    "plBedding": false,
    "searchObj": {
        "date": "31012017",
        "searchFrom": {
            "id": 2001131,
            "name": "Большая Волга"
        },
        "searchTo": {
            "id": 2001026,
            "name": "Дубна"
        }
    },
    "trainInfo": {
        "arrival": {
            "code": "2001026",
            "country": {
                "code": 20,
                "name": {
                    "en": "Russia",
                    "ru": "Россия"
                }
            },
            "local": false,
            "time": "2017-01-31T19:33:00"
        },
        "carTypes": [
            {
                "commissions": [
                    {
                        "amount": 0,
                        "cost": 1,
                        "currency": "RUB",
                        "type": "percent_markup"
                    }
                ],
                "cost": 31,
                "freeSeats": 489,
                "gdsCost": 30,
                "type": 2,
                "typeName": "Сидячий"
            }
        ],
        "class": [
            "скорый"
        ],
        "departure": {
            "code": "2001131",
            "country": {
                "code": 20,
                "name": {
                    "en": "Russia",
                    "ru": "Россия"
                }
            },
            "local": false,
            "stopInMinutes": 3,
            "time": "2017-01-31T19:23:00"
        },
        "durationInMinutes": 10,
        "elReg": true,
        "number": "834М",
        "number2": "7034М",
        "route": [
            "Москва савеловская",
            "Дубна"
        ],
        "trainNumber": "834М"
    },
    "type": "bookStart"
}
```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Wed, 25 Jan 2017 13:23:27 GMT
Set-Cookie: _sst=1485350606152|1485350606152|bdb2dd6e3c544bbe2abac134734306b4; path=/
Set-Cookie: sid=uyrCJyRh/NeprZt2LpICqpJt; expires=Thu, 26-Jan-2017 13:23:26 GMT; path=/
Set-Cookie: sid=uyrCJyRh/NeprZt2LpICqpJt; expires=Thu, 26-Jan-2017 13:23:26 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "result": {
        "bookId": "b3305150-06f9-43f1-adb1-2b98fef18679"
    },
    "success": true
}
```


Запрос на старт бронирования.

Бронирование реализовано в две стадии: запрос на бронирование, получение статуса бронирования.

### HTTP Request

`POST https://www.onetwotrip.com/_api/rzd/bookManager`

### Required params

Параметр | Описание
--------- | -----------
type | **bookStart** обязательный параметр-ключ
buyerInfo | информация о покупателе (mail, phone)
searchObj | информация о маршруте
carType | тип вагона
trainInfo | информация о поезде
trainNum | номер поезда (для запроса использовать значение из trainNumber2)
carNum | номер вагона
paxes | список пассажиров
docType | тип документа
docNumber | номер документа
country | страна, выдавшая документ
birthDate | дата рождения
birthPlace | место рождения
sex | пол пассажира
plBedding | необязательный, учитывается только в случае если поезд едет российским перевозчиком и плацкарт - покупать ли постельное белье всем пассажирам (Boolean)

### Required params for blocked locations

Параметр | Описание
--------- | -----------
device_id | идентификатор девайса
device_name | название девайса
lat | широта
long | долгота

## getStatus

> Request

```http
GET /rzd/bookManager?type=getStatus&id=b3305150-06f9-43f1-adb1-2b98fef18679 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: onetwotrip.com
```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Wed, 25 Jan 2017 14:15:07 GMT
Set-Cookie: _sst=1485353707159|1485353707159|5e0196e4b7b55ba692e31241e95d775c; path=/
Set-Cookie: sid=FVWuIo+g5Rhw5oIouffpc/oK; expires=Thu, 26-Jan-2017 14:15:07 GMT; path=/
Set-Cookie: sid=FVWuIo+g5Rhw5oIouffpc/oK; expires=Thu, 26-Jan-2017 14:15:07 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "success": true,
    "result": {
        "bookPageUrl": "http://www.onetwotrip.com/ru/railways/%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0-2000000_%D0%A1%D0%B0%D0%BD%D0%BA%D1%82-%D0%9F%D0%B5%D1%82%D0%B5%D1%80%D0%B1%D1%83%D1%80%D0%B3-2004000/24.12.14/116%D0%A1-3/4/20",
        "bookItemsIds": [
            "6f754b03-4ee5-46db-8167-3e33a11f55d0"
        ],
        "engineType": "simple_api",
        "additionalServices": {
            "insurance_railways": {
                "types": [
                  {
                    "type": "ACCIDENT",
                    "cost": 150,
                    "currency": "RUB",
                    "payouts": [
                      150000
                    ],
                    "payout": 1.5
                  }
                ],
                "currency": "RUB",
                "paymentVariants": [
                  {
                    "type": "card",
                    "tag": "payture_otk_railways"
                  }
                ],
                "source": "ALFA_RAILWAYS"
            }
        },
        "additionalServicesStatus": {
          "insurance_railways": {
            "status": "COMPLETE",
            "statuses": [
              {
                "status": "BLOCKED",
                "date": "2016-11-25T10:16:42"
              },
              {
                "status": "COMPLETE",
                "date": "2016-11-25T10:17:37"
              }
            ]
          }
        },
        "messages": ["SHOW_INSURANCE"],
        "ttl": 491,
        "trip": {
            "from": 2000000,
            "fromName": "Москва",
            "to": 2004000,
            "toName": "Санкт-Петербург",
            "date": "12.11.2014",
            "trainInfo": {
                "number": "016А",
                "class": [
                    "скорый",
                    "фирменный"
                ],
                "durationInMinutes": 481,
                "route": [
                    "Москва",
                    "Мурманск"
                ],
                "departure": {
                    "time": "2014-11-12T00:43:00",
                    "local": false,
                    "station": "Ленинградский вокзал"
                },
                "arrival": {
                    "time": "2014-11-12T08:44:00",
                    "local": false,
                    "stopInMinutes": 29
                },
                "name": "Арктика"
            },
            "train": "016А",
            "carType": 3,
            "car": 7,
            "departure": "12.11.2014T00:43",
            "arrival": "12.11.2014T08:44"
        },
        "amount": 1456.8,
        "gdsAmount": 1300,
        "passengers": [
            {
                "name": {
                    "first": "Фамилия",
                    "last": "Имя",
                    "mid": "Отчество"
                },
                "docType": 1,
                "docNumber": "4606123456",
                "country": "RU",
                "birthDate": "1984-11-28T00:00:00",
                "birthPlace": "Москва",
                "sex": 1,
                "seats": [52],
                "tariffType": 0,
                "status": "Reserved",
                "statuses": [
                    {
                        "status": "New",
                        "date": "2014-11-11T10:49:31"
                    },
                    {
                        "status": "Reserved",
                        "date": "2014-11-11T10:50:51"
                    }
                ],
                "policyLink": "http://web-ins.vtsft.ru/travel-ext-services/reports?parameters=0C2EA8FC818FAD51DD633184D68D42771C509C4C2F7FFF006E61B4E57A358E452083DBCC60824DC49E7622310B992992", // ссылка на страховой полис
                "amount": 1456.8,
                "gdsAmount": 1300,
                "currency": "RUB",
                "place": 52
            }
        ],
        "buyer": {
            "mail": "yocto@mail.ru",
            "phone": "79060882888",
            "bid": "24826ec1-0080-4944-8b7a-aed58d2a7f52"
        }
    }
}
```

Получение результатов по ранее выполненному запросу на бронирование жд билетов по ключу (bookId). Бронирование может быть в двух состояниях: оформляется  ("In progress") и оформлено.

### HTTP Request

`GET $API_HOST/_api/rzd/bookManager?type=getStatus&id={String}`

### Params

Параметр | Описание
--------- | -----------
type | **getStatus** обязательный параметр
id | ID брони

### Response description
Поле | Описание
--------- | -----------
bookItemsIds  | номера броней
engineType  |  название поставщика
additionalServices  |  дополнительные услуги
additionalServices.types  |  варианты страховки
additionalServices.cost |  сколько стоит страховка
additionalServicesStatus  |  статус доп услуг
messages |  какие сообщения необходимо показывать (страховку в данном случае)   
ttl |  время жизни брони в сек
trip.from | id станции отправления
trip.fromName | название станции отправления
trip.to | id станции прибытия
trip.toName | название станции прибытия
trip.date | дата отправления
trip.trainInfo | информация о поезде
trip.trainInfo.number | номер
trip.trainInfo.class | список классов поезда
trip.trainInfo.durationInMinutes | время в пути
trip.trainInfo.route | маршрут поезда (может быть три пункта)
trip.trainInfo.departure.time |  дата и время отправления
trip.trainInfo.departure.station | название вокзала (необязательно)
trip.trainInfo.arrival.time |  дата и время прибытия
trip.trainInfo.arrival.stopInMinutes |  время остановки (необязательно)
trip.trainInfo.train | номер поезда
trip.trainInfo.carType | тип вагона
trip.trainInfo.car | номер вагона
trip.trainInfo.departure |  дата и время отправления
trip.trainInfo.arrival |  дата и время прибытия
trip.amount |  сумма заказа
trip.gdsAmount |  стоимость поставщика без нашей наценки
trip.passengers | список пассажиров
trip.passengers.name |  ФИО пассажира
trip.passengers.docType |  тип документа
trip.passengers.docNumber |  номер документа
trip.passengers.country |  страна выдачи документа
trip.passengers.birthDate |  дата рождения
trip.passengers.birthPlace |  место рождения
trip.passengers.sex | пол
trip.passengers.seats | запрошенное место
trip.passengers.tariffType |  тип тарифа
trip.passengers.status |  статус билета пассажира
trip.passengers.statuses |      история изменения статусов
trip.passengers.policyLink | ссылка на страховой полис (необязательно)
trip.passengers.amount |  стоимость билета с наценкой
trip.passengers.gdsAmount 1300 |  без наценки
trip.passengers.currency RUB |  валюта
trip.passengers.place 52 |  полученное место
trip.buyer  |  информация о покупателе


## cancelBooking

Отмена бронирования.

> Request

```http
GET /_api/rzd/cancelBooking?confirmationId=4af760b7-bbbe-474c-8135-8b8924d95dba HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: dev-linode-00--www.onetwotrip.com
User-Agent: HTTPie/0.9.2
```

> Response

```http

HTTP/1.1 200 OK
Cache-control: private
Content-Encoding: gzip
Content-Type: application/json;charset=utf-8
Date: Fri, 27 Jan 2017 08:55:08 GMT
Server: nginx/1.11.3
Set-Cookie: _sst=1485507306331|1485507306331|37af681cc4c883f648408a660b6e6d1d; path=/; Secure
Set-Cookie: ENVID=dev-linode-00|WIsK7; path=/; Secure
Transfer-Encoding: chunked

{
    "result": "OK",
    "success": true
}

```

### HTTP Request

`GET $API_HOST/_api/rzd/cancelBooking?confirmationId={String}`

### Params

Параметр | Описание
--------- | -----------
confirmationId | ID брони - `bookId` из результата bookManager?type=bookStart

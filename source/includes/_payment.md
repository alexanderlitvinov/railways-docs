# payment

## createOrder

Создание заказа и оплата уже существующего заказа

> Request

```http
POST /_api/rzd/createOrder HTTP/1.1
Host: www.onetwotrip.com
Connection: keep-alive
Content-Length: 664
Pragma: no-cache
Cache-Control: no-cache
Origin: https://www.onetwotrip.com
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36
Content-Type: application/json
Accept: */*
Referer: https://www.onetwotrip.com/ru/poezda/pay/9c2f67a6-b963-4776-897d-fd451aa3bdea?metaFrom=23763&metaTo=23364&date=09022017&fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&seats%5B0%5D%5Bplace%5D=74&seats%5B0%5D%5BdocNum%5D=II%D0%91%D0%A6123123
Accept-Encoding: gzip, deflate, br
Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4
Cookie: src_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; session_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; referrer_hist=dir; _gat=1; _visitordata=u5hck0t8mt650gl53rkmdhfl1jbqnub9qp6pncaj9pl43amo0p3ne0vug8141a5asheaj5kid2djm===; km_user_packages_id=189ea1c9-817d-073e-f001-ec03c1b07957; accept_language=ru; km_uq=1486048657%20%2Fe%3FcityFrom%3D%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0%26cityTo%3D%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0%26date%3D09.02.2017T19%253A23%26carClass%3D%25D0%25A1%25D0%25B8%25D0%25B4%25D1%258F%25D1%2587%25D0%25B8%25D0%25B9%26carNumber%3D02%26trainNumber%3D834%25D0%259C%26gdsAmount%3D7.5%26amount%3D7.5%26reservationID%3Db243fae6-7a8d-4f1c-b1b9-aead685cc350%26confirmationID%3D9c2f67a6-b963-4776-897d-fd451aa3bdea%26engineType%3Dsimple_api%26vid%3Dundefined%26bid%3Dundefined%26referrer%3Dundefined%26abst%3Dundefined%26Language%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_BOOK_SUCCESS%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657%7C1486048657%20%2Fe%3FconfirmationID%3D9c2f67a6-b963-4776-897d-fd451aa3bdea%26vid%3Dundefined%26bid%3Dundefined%26referrer%3Dundefined%26abst%3Dundefined%26Language%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_PAY_SHOW%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657%7C1486048657%20%2Fe%3FLanguage%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3Dvas_insurance_show%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657; _sst=1486048642933|1486048662215|e07c619c2845d0236556c8e0097e3a6a; sid=T5oTMwKZVSCFFsroXjCqaxI8

{
    "bookId": "9c2f67a6-b963-4776-897d-fd451aa3bdea",
    "successRedirect": "https://www.onetwotrip.com/ru/poezda/ticket/9c2f67a6-b963-4776-897d-fd451aa3bdea/?metaFrom=23763&metaTo=23364&date=09022017&fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0",
    "failureRedirect": "https://www.onetwotrip.com/ru/poezda/pay/9c2f67a6-b963-4776-897d-fd451aa3bdea/?metaFrom=23763&metaTo=23364&date=09022017&fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0",
    "noRedirect": false,
    "card": {
        "id": "09f94b90-c7bf-43ac-9f9f-fc779d4b6f3a",
        "cvv": "123"
    }
}
```

> Additional services request

```http
POST /_api/rzd/createOrder HTTP/1.1
Host: www.onetwotrip.com
Connection: keep-alive
Content-Length: 664
Pragma: no-cache
Cache-Control: no-cache
Origin: https://www.onetwotrip.com
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36
Content-Type: application/json
Accept: */*
Referer: https://www.onetwotrip.com/ru/poezda/pay/9c2f67a6-b963-4776-897d-fd451aa3bdea?metaFrom=23763&metaTo=23364&date=09022017&fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&seats%5B0%5D%5Bplace%5D=74&seats%5B0%5D%5BdocNum%5D=II%D0%91%D0%A6123123
Accept-Encoding: gzip, deflate, br
Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4
Cookie: src_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; session_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; referrer_hist=dir; _gat=1; _visitordata=u5hck0t8mt650gl53rkmdhfl1jbqnub9qp6pncaj9pl43amo0p3ne0vug8141a5asheaj5kid2djm===; km_user_packages_id=189ea1c9-817d-073e-f001-ec03c1b07957; accept_language=ru; km_uq=1486048657%20%2Fe%3FcityFrom%3D%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0%26cityTo%3D%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0%26date%3D09.02.2017T19%253A23%26carClass%3D%25D0%25A1%25D0%25B8%25D0%25B4%25D1%258F%25D1%2587%25D0%25B8%25D0%25B9%26carNumber%3D02%26trainNumber%3D834%25D0%259C%26gdsAmount%3D7.5%26amount%3D7.5%26reservationID%3Db243fae6-7a8d-4f1c-b1b9-aead685cc350%26confirmationID%3D9c2f67a6-b963-4776-897d-fd451aa3bdea%26engineType%3Dsimple_api%26vid%3Dundefined%26bid%3Dundefined%26referrer%3Dundefined%26abst%3Dundefined%26Language%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_BOOK_SUCCESS%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657%7C1486048657%20%2Fe%3FconfirmationID%3D9c2f67a6-b963-4776-897d-fd451aa3bdea%26vid%3Dundefined%26bid%3Dundefined%26referrer%3Dundefined%26abst%3Dundefined%26Language%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_PAY_SHOW%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657%7C1486048657%20%2Fe%3FLanguage%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3Dvas_insurance_show%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657; _sst=1486048642933|1486048662215|e07c619c2845d0236556c8e0097e3a6a; sid=T5oTMwKZVSCFFsroXjCqaxI8

{
    "bookId": "6405e863-315f-4b96-a8fe-5f5329c36e99",
    "card": {
        "num":  "4111111111111111",
        "date": "11/17",
        "holder": "OLEG IVANOV",
        "cvv": "306"
    },
    "additionalServices": {
        "insurance_railways": {
            "types": [
                {
                    "type": "ACCIDENT",
                    "cost": 150,
                    "currency": "RUB",
                    "payouts": [
                        150000
                    ]
                }
            ]
        }
    }
}
```

> Apple pay request

```http
POST /_api/rzd/createOrder HTTP/1.1
Host: www.onetwotrip.com
Connection: keep-alive
Content-Length: 664
Pragma: no-cache
Cache-Control: no-cache
Origin: https://www.onetwotrip.com
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36
Content-Type: application/json
Accept: */*
Referer: https://www.onetwotrip.com/ru/poezda/pay/9c2f67a6-b963-4776-897d-fd451aa3bdea?metaFrom=23763&metaTo=23364&date=09022017&fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&seats%5B0%5D%5Bplace%5D=74&seats%5B0%5D%5BdocNum%5D=II%D0%91%D0%A6123123
Accept-Encoding: gzip, deflate, br
Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4
Cookie: src_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; session_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; referrer_hist=dir; _gat=1; _visitordata=u5hck0t8mt650gl53rkmdhfl1jbqnub9qp6pncaj9pl43amo0p3ne0vug8141a5asheaj5kid2djm===; km_user_packages_id=189ea1c9-817d-073e-f001-ec03c1b07957; accept_language=ru; km_uq=1486048657%20%2Fe%3FcityFrom%3D%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0%26cityTo%3D%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0%26date%3D09.02.2017T19%253A23%26carClass%3D%25D0%25A1%25D0%25B8%25D0%25B4%25D1%258F%25D1%2587%25D0%25B8%25D0%25B9%26carNumber%3D02%26trainNumber%3D834%25D0%259C%26gdsAmount%3D7.5%26amount%3D7.5%26reservationID%3Db243fae6-7a8d-4f1c-b1b9-aead685cc350%26confirmationID%3D9c2f67a6-b963-4776-897d-fd451aa3bdea%26engineType%3Dsimple_api%26vid%3Dundefined%26bid%3Dundefined%26referrer%3Dundefined%26abst%3Dundefined%26Language%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_BOOK_SUCCESS%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657%7C1486048657%20%2Fe%3FconfirmationID%3D9c2f67a6-b963-4776-897d-fd451aa3bdea%26vid%3Dundefined%26bid%3Dundefined%26referrer%3Dundefined%26abst%3Dundefined%26Language%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_PAY_SHOW%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657%7C1486048657%20%2Fe%3FLanguage%3Dru%26EventHour%3D18%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D189ea1c9-817d-073e-f001-ec03c1b07957%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3Dvas_insurance_show%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DpkmtIP9bJ9yaDti7zJW2PXZOUlI%253D%26_t%3D1486048657; _sst=1486048642933|1486048662215|e07c619c2845d0236556c8e0097e3a6a; sid=T5oTMwKZVSCFFsroXjCqaxI8

{
    "bookId": "6405e863-315f-4b96-a8fe-5f5329c36e99",
    "paymentToken": {
        "version": "EC_v1",
        "data": "xtPkihU93Z0gWYL0xATR733ODE5uVfJCF2x0M3wNtSfXi3SVMRPbanQsPt+0OJE84HEgflz9TIityPLx71tFQYKle1bf+tMl9ORfRx+9Ef+ERJSyYNjn6sTT0Rpy1J6XvvYBiKJmvPCiJRGmckiGheeGTkKb2yLygeZBAl/H5CQ+z69w8b5pQz0o+flfvst77oQRc2lqk8INkgmyP3SyCW1q2nX+ZzJshfvh6IVsX7gzW5i1iQSmTeVozu8dXbGmOg3tve8sS9HF5TQVUXerlNKcjp5C1bT4+hWzA0/PSE0BoCWTpSOkL8FG5n4MIaMBoS7tcOEaZ8SFK4dlf7gpzalXDeR6lWoueXwU0ak61XXoOFoqQNJ33BamzdJtyvlkeOULO0TLq8hOnb1UUQ==",
        "signature": "MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwEAAKCAMIID4jCCA4igAwIBAgIIJEPyqAad9XcwCgYIKoZIzj0EAwIwejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDkyNTIyMDYxMVoXDTE5MDkyNDIyMDYxMVowXzElMCMGA1UEAwwcZWNjLXNtcC1icm9rZXItc2lnbl9VQzQtUFJPRDEUMBIGA1UECwwLaU9TIFN5c3RlbXMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwhV37evWx7Ihj2jdcJChIY3HsL1vLCg9hGCV2Ur0pUEbg0IO2BHzQH6DMx8cVMP36zIg1rrV1O/0komJPnwPE6OCAhEwggINMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDEwHQYDVR0OBBYEFJRX22/VdIGGiYl2L35XhQfnm1gkMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUI/JJxE+T5O8n5sT2KGw/orv9LkswggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMA4GA1UdDwEB/wQEAwIHgDAPBgkqhkiG92NkBh0EAgUAMAoGCCqGSM49BAMCA0gAMEUCIHKKnw+Soyq5mXQr1V62c0BXKpaHodYu9TWXEPUWPpbpAiEAkTecfW6+W5l0r0ADfzTCPq2YtbS39w01XIayqBNy8bEwggLuMIICdaADAgECAghJbS+/OpjalzAKBggqhkjOPQQDAjBnMRswGQYDVQQDDBJBcHBsZSBSb290IENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xNDA1MDYyMzQ2MzBaFw0yOTA1MDYyMzQ2MzBaMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABPAXEYQZ12SF1RpeJYEHduiAou/ee65N4I38S5PhM1bVZls1riLQl3YNIk57ugj9dhfOiMt2u2ZwvsjoKYT/VEWjgfcwgfQwRgYIKwYBBQUHAQEEOjA4MDYGCCsGAQUFBzABhipodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDA0LWFwcGxlcm9vdGNhZzMwHQYDVR0OBBYEFCPyScRPk+TvJ+bE9ihsP6K7/S5LMA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUu7DeoVgziJqkipnevr3rr9rLJKswNwYDVR0fBDAwLjAsoCqgKIYmaHR0cDovL2NybC5hcHBsZS5jb20vYXBwbGVyb290Y2FnMy5jcmwwDgYDVR0PAQH/BAQDAgEGMBAGCiqGSIb3Y2QGAg4EAgUAMAoGCCqGSM49BAMCA2cAMGQCMDrPcoNRFpmxhvs1w1bKYr/0F+3ZD3VNoo6+8ZyBXkK3ifiY95tZn5jVQQ2PnenC/gIwMi3VRCGwowV3bF3zODuQZ/0XfCwhbZZPxnJpghJvVPh6fRuZy5sJiSFhBpkPCZIdAAAxggFeMIIBWgIBATCBhjB6MS4wLAYDVQQDDCVBcHBsZSBBcHBsaWNhdGlvbiBJbnRlZ3JhdGlvbiBDQSAtIEczMSYwJAYDVQQLDB1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMCCCRD8qgGnfV3MA0GCWCGSAFlAwQCAQUAoGkwGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTYxMDAzMTUyNTAwWjAvBgkqhkiG9w0BCQQxIgQgc8WfwzJL89CsP3av6GJ4USfLdQzHz4oNm0PutwBMszkwCgYIKoZIzj0EAwIERjBEAiAynR8Q7ULKZwmQuNtGc+j6luCxEJjRm/yB94gJhvDoSQIgB3Tclkqy5xmIV8sIX+gnFVr1wNl2jSk6V9muTx9qKn4AAAAAAAA=",
        "header": {
            "ephemeralPublicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEf2V2pOgsn9krngbcgk+ifNXRwtdRiE7t0rbstCvRRxcoiHbbRdSVOvdKU+syDbZC1ygWVrg9WsPJHE6odQqo9Q==",
            "publicKeyHash": "NsiQQYoWrblLOco+9hLBySty0GRhz6/M5PjNnepIv5M=",
            "transactionId": "75223b238752efd010382b2d97025676de1eeb47a165f851eba53835c04cbcd7"
        }
    }
}
```

> 3ds Response

```http
HTTP/1.1 200 OK
Server: nginx/1.8.1
Date: Thu, 02 Feb 2017 17:29:23 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive
Vary: Accept-Encoding
Set-Cookie: _sst=1486048642933|1486101764361|e07c619c2845d0236556c8e0097e3a6a; path=/
Set-Cookie: sid=T5oTMwKZVSCFFsroXjCqaxI8; expires=Sat, 4-Feb-2017 06:02:44 GMT; path=/; domain=.onetwotrip.com
Content-Encoding: gzip

{
  "success": true,
  "result": {
    "type": "3ds",
    "formUrl": "https://acs3.sbrf.ru:443/acs_a/pa?id=WJQc6UrAedGnAZHRF6GMCg",
    "sid": "58d62200-f93c-4ae2-b93e-0b9ee69ff819",
    "formData": {
      "PaReq": "eJxVUmFvokAQ/SvG72WHBYEz4yZcMUp7gFGbJv3G4VYxBXRZKvjru2vhvH57b2by9s2bxe1BcB5s\r\neNYIzjDidZ3u+SjfzcYUTBcoWOAAtT34RU3PGjNc+Wt+ZvjJRZ1XJTMNMCiSgSoFkR3SUjJMs/Pv\r\nMGa261DwkPQUCy7CgAGAPTFNagOYAO7DAJB897FMC86SeL59TbbrcIXkVsCsakopOubZgGQg2IgP\r\ndpDyVE8JOaWdVLsYWVUQJLqD5G5q1WhUK6U237Ho6Lfx1e+iILrER7+Lg/0l2YZtEkQzJHoCd6nk\r\nbIhiBM4U6NRW69zqmBbaAnONibLTEzzpN/y+oxv/F1AFLXiZdcyxLbXCwJC3p6rkakKl+Q8juRt+\r\nXOpMM6nSiZr3RXt+S+n8Kf+7kX94tliun68vFlTKdz+kFXOVjumZykNPkGgZ0h9RBXO7t0I//sEX\r\nFEetDw==",
      "MD": "20170203060248092183",
      "TermUrl": "https://www.onetwotrip.com/_api/rzd/orderManager?type=backFromPayment&sid=58d62200-f93c-4ae2-b93e-0b9ee69ff819&jSessionId=&orderId=07904d9f-c7d8-4fbd-884f-232ea3e5ff3a&gdsOrderId=229458605&payId=f53687b1-c221-4aa2-954a-e41e07128686&success-metaFrom=23763&success-metaTo=23364&success-date=08022017&success-fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&success-toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&successRedirect=https%3A%2F%2Fwww.onetwotrip.com%2Fru%2Fpoezda%2Fticket%2F8d3415c4-812a-4f7d-84a1-8a2cda6f2380%2F&failure-metaFrom=23763&failure-metaTo=23364&failure-date=08022017&failure-fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&failure-toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&failureRedirect=https%3A%2F%2Fwww.onetwotrip.com%2Fru%2Fpoezda%2Fpay%2F8d3415c4-812a-4f7d-84a1-8a2cda6f2380%2F&paymentId=ca3398e1-531f-4edf-b22d-8b5d121a3aff"
    },
    "paymentId": "ca3398e1-531f-4edf-b22d-8b5d121a3aff",
    "provider": "avia",
    "orderNumber": "H03432564",
    "orderId": "07904d9f-c7d8-4fbd-884f-232ea3e5ff3a"
  }
}
```

> non-3ds Response

```http
HTTP/1.1 200 OK
Server: nginx/1.8.1
Date: Thu, 02 Feb 2017 15:04:58 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive
Vary: Accept-Encoding
Set-Cookie: _sst=1486048642933|1486048662333|e07c619c2845d0236556c8e0097e3a6a; path=/
Set-Cookie: sid=T5oTMwKZVSCFFsroXjCqaxI8; expires=Fri, 3-Feb-2017 15:17:42 GMT; path=/; domain=.onetwotrip.com
Content-Encoding: gzip

{
  "success": true,
  "result": {
    "non3ds": true,
    "PaRes": 1,
    "type": "non3ds",
    "formUrl": "https://www.onetwotrip.com/_api/rzd/redirectToBackFromPayment",
    "sid": "9ef0cbc4-ada0-4664-90d3-ee4f1d29f19d",
    "formData": {
      "PaReq": "1",
      "MD": "1",
      "TermUrl": "https://www.onetwotrip.com/_api/rzd/orderManager?type=backFromPayment&sid=9ef0cbc4-ada0-4664-90d3-ee4f1d29f19d&jSessionId=&orderId=41e894f0-c1c9-4e17-9350-4c7e84b20150&gdsOrderId=229399338&payId=5efb0baa-b298-487a-a493-2399a5b3ded7&success-metaFrom=23763&success-metaTo=23364&success-date=09022017&success-fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&success-toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&successRedirect=https%3A%2F%2Fwww.onetwotrip.com%2Fru%2Fpoezda%2Fticket%2F9c2f67a6-b963-4776-897d-fd451aa3bdea%2F&failure-metaFrom=23763&failure-metaTo=23364&failure-date=09022017&failure-fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&failure-toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&failureRedirect=https%3A%2F%2Fwww.onetwotrip.com%2Fru%2Fpoezda%2Fpay%2F9c2f67a6-b963-4776-897d-fd451aa3bdea%2F&non3ds=true&PaRes=1&paymentId=b243fae6-7a8d-4f1c-b1b9-aead685cc350"
    },
    "paymentId": "b243fae6-7a8d-4f1c-b1b9-aead685cc350",
    "provider": "avia",
    "orderNumber": "H03365862",
    "orderId": "41e894f0-c1c9-4e17-9350-4c7e84b20150"
  }
}
```

### HTTP Request

`POST $API_HOST/_api/rzd/createOrder`

### Params

Параметр | Описание
--------- | -----------
bookId | ID брони
successRedirect | URL на который будет redirect в случае успеха
failureRedirect | URL на который будет redirect в случае ошибки
noRedirect | Не делать redirect
card | Банковская карта
paymentToken | Apple pay token
fullBonuses | Флаг - полная оплата бонусами, влияет только на необходимость отправки данных карты
fullRefundAccount | Флаг - полная оплата с refundAccount, влияет только на необходимость отправки данных карты
bonusAmount | Сумма оплаты бонусами
bonusCurrency | Валюта бонусов RUB/MLS
refundAccountAmount | Сумма для оплаты с refundAccount
checkBy3ds | Использовать 3ds - по-умолчанию true
additionalServices | Доп услуги
addCard | Сохранить карту
flags.failInsurance | Для теста отказа оформления страховки
flags.testPg | Тестовая оплата

<aside class="notice">
    На данный момент частичная оплата с refundAccount не поддежривается, следует передавать refundAccountAmount == amount брони
</aside>

### Card сохраненная карта

Параметр | Описание
--------- | -----------
id | ID сохраненной карты
cvv | Секретный код

### Card новая карта

Параметр | Описание
--------- | -----------
cvv | Секретный код
date | Дата окончания срока действия в формате MM/YY
holder | Имя владельца карты
num | Номер карты

### Response description
Поле | Описание
--------- | -----------
non3ds | true если не было 3ds
PaRes | ??
type | non3ds / 3ds
sid | session id == gdsBookId
formUrl | url на который нужно сделать POST с formData
formData | Данные формы для отправки на сайт 3ds
formData.TermUrl | Ссылка куда вернемся с 3ds
paymentId | ID платежа - нету на ржд оплате
provider | avia
orderNumber | Номер заказа
orderId | ID заказа

## backFromPayment

Возврат с 3ds, подтверждение покупки

> Request

```http
POST /_api/rzd/orderManager?type=backFromPayment&sid=58d62200-f93c-4ae2-b93e-0b9ee69ff819&jSessionId=&orderId=07904d9f-c7d8-4fbd-884f-232ea3e5ff3a&gdsOrderId=229458605&payId=f53687b1-c221-4aa2-954a-e41e07128686&success-metaFrom=23763&success-metaTo=23364&success-date=08022017&success-fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&success-toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&successRedirect=https%3A%2F%2Fwww.onetwotrip.com%2Fru%2Fpoezda%2Fticket%2F8d3415c4-812a-4f7d-84a1-8a2cda6f2380%2F&failure-metaFrom=23763&failure-metaTo=23364&failure-date=08022017&failure-fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&failure-toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0&failureRedirect=https%3A%2F%2Fwww.onetwotrip.com%2Fru%2Fpoezda%2Fpay%2F8d3415c4-812a-4f7d-84a1-8a2cda6f2380%2F&paymentId=ca3398e1-531f-4edf-b22d-8b5d121a3aff HTTP/1.1
Host: www.onetwotrip.com
Connection: keep-alive
Content-Length: 5602
Pragma: no-cache
Cache-Control: no-cache
Origin: https://acs3.sbrf.ru
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Referer: https://acs3.sbrf.ru/acs_a/uidispatcher;jsessionid=0FB632090A439F02384F6BC93D418B57
Accept-Encoding: gzip, deflate, br
Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4
Cookie: src_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; session_ref=https://www.onetwotrip.com/ru/poezda/pay/8795fb20-8bc7-4528-ab09-47360593ac7a?metaFrom=23763&metaTo=23364&date=09022017&fromName=%25D0%2591%25D0%25BE%25D0%25BB%25D1%258C%25D1%2588%25D0%25B0%25D1%258F%2520%25D0%2592%25D0%25BE%25D0%25BB%25D0%25B3%25D0%25B0&toName=%25D0%2594%25D1%2583%25D0%25B1%25D0%25BD%25D0%25B0&seats%255B0%255D%255Bplace%255D=74&seats%255B0%255D%255BdocNum%255D=II%25D0%2591%25D0%25A6123123; referrer_hist=dir; referrer=; km_ai=cBHvIdi8piOxKu9mORyMflaaH6s%3D; tvc=1; kvcd=1486049914053; km_lv=1486049914; cookiePolicyHasAccepted=true; _ga=GA1.2.59341541.1486048679; km_lv=1486049998; gaTransactionOrderId=b01c72e8-749f-45cd-851a-f08a1f406e08; km_user_packages_id=11f0cfc9-c429-ad10-0dec-6ebbf1cd0a3b; _gat=1; accept_language=ru; vid=c379e36e-9dc4-4bea-acca-abfbc4064472; abst=exp1_x; _sst=1486048642933|1486101764361|e07c619c2845d0236556c8e0097e3a6a; sid=T5oTMwKZVSCFFsroXjCqaxI8; km_uq=1486101739%20%2Fe%3FconfirmationID%3D8d3415c4-812a-4f7d-84a1-8a2cda6f2380%26vid%3Dc379e36e-9dc4-4bea-acca-abfbc4064472%26bid%3Dundefined%26referrer%3D%26abst%3Dexp1_x%26Language%3Dru%26EventHour%3D9%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D11f0cfc9-c429-ad10-0dec-6ebbf1cd0a3b%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_PAY_SHOW%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DcBHvIdi8piOxKu9mORyMflaaH6s%253D%26_t%3D1486101739%7C1486101739%20%2Fe%3FLanguage%3Dru%26EventHour%3D9%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D11f0cfc9-c429-ad10-0dec-6ebbf1cd0a3b%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3Dvas_insurance_show%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DcBHvIdi8piOxKu9mORyMflaaH6s%253D%26_t%3D1486101739%7C1486101775%20%2Fe%3FreservationID%3Dca3398e1-531f-4edf-b22d-8b5d121a3aff%26confirmationID%3D8d3415c4-812a-4f7d-84a1-8a2cda6f2380%26gdsAmount%3D7.5%26amount%3D7.5%26insurance_rzd%3Dfalse%26vid%3Dc379e36e-9dc4-4bea-acca-abfbc4064472%26bid%3Dundefined%26referrer%3D%26abst%3Dexp1_x%26Language%3Dru%26EventHour%3D9%26user_agent%3DMozilla%252F5.0%2520(Macintosh%253B%2520Intel%2520Mac%2520OS%2520X%252010_12_2)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F55.0.2883.95%2520Safari%252F537.36%26KM_user_id%3D11f0cfc9-c429-ad10-0dec-6ebbf1cd0a3b%26lang%3Dru%26locale%3Dru%26c_email%3Dnot_logged%26_n%3DRZD_PAY_3DS%26_k%3D8d1f8584cbc0a025b95e59b6a64a8e66f864680f%26_p%3DcBHvIdi8piOxKu9mORyMflaaH6s%253D%26_t%3D1486101775

PaRes=eJzNWdnOo8iSfpVWz6XVzW6b1l%2B%2FlOyLwYBZjO%2FYzA5msVmeftJ2VXV1nT4zZ87F0ViySILIyIiM5YuEDzvrk4Q7JdG9Tz4%2FtGQYgjT5JY%2B%2F%2FIqj2A7FUQLdoji5R2kc2xO%2Ffn4YwEqGF4OnmNF2mxy7FOtUc%2B32tyOvAsjySPohb5tP7Hf0d%2FwD%2BXYLpfdRFjTj50cQdYysf5K7LY7uP5Cvtx910svcJ4qiJIVhOImiGIrufvs2%2BEDezz%2BQPwUZ9%2BdogKrPefypFWDWV7BonDbpBVh0Lp2OtjwfOe3LB%2FLk%2BIiDMfn8Ztov6PYPFP%2BDhCq86B%2B3pzhQt3coe0ehH8iPhA%2B4R33SRMvnliQ%2BkO93H8l8a5sEckBjv48%2FkD91uwXN06rvP2yHwSWf1A%2F7%2FPkx5vVPOhF%2F4JDhRf8YxmC8D5%2F%2BB%2FJ19BEFj8cnAIABpxIXWVCl9UUUbd8H7x%2B09cXykUT5J0pBpeD1NQuytn0%2BZvVT1b8SPpCnKsjLvZ8fpzxt4GJ98stcV83w5ddsHG9%2FIMg0Tb9PxO9tnyI4NARBaQQyxEOe%2Ftev71lJLDfX9v80jQ2atsmjoMrXYISRoiVj1sa%2FfNft78TY1lMShlg8%2BxsU9VuEkc1vTwpKYBSUify90B8s%2B1dW%2BVnZfgh%2BG7IAey7wk6DPDyu5Js%2BISH5xLPnLr%2F%2F1T%2FODy9NkGP%2Bd9b%2Bt%2FaOEb%2FLcoLonn0TTFpIknWHQz22BWSh36OI%2BJYRI%2FvJt3pvzA%2Fmu8Fdr3q77YYvejBhRHDe7HB8ngDeBLuuPyilqTucZFx1uI%2BWoJVqpVbzc0Rr38XK3HFiQHYdcLfeNZO7ynXRqb%2FdLlaqYfUN09hK5bt535gmcqFuDXlrX2h6mS2OuE36zirDcXpDFKYd6W2YgycbelTe7%2B3lcy%2B5qI%2Bu1v80ndNMZQO%2BURL3Tjuj2ix%2FSV0k0KvnSHhmt29zUbsA3pva4KAmVPh5zYp73WJjYogcexMKH8Roc6etgE7eRR%2FZr7yTnblCXIPMdd7XIo3dFg5y7lee6OBJrknp9xyPBxbC4PcpUsrvjHxsSR%2FmcyrRsZoJcaXb5YaYKX1YLOzwHxcaZTeSyje95wzbYTe%2FYu7dj8kWkdpz55csPIfTVI2qyvD1wplCaC8bgPWKTfsyvMJZhidJkWfALlmX4LgWTzIBUNu3mhF0cBbWovGbVA8fj6sFZgc6kZZeVuUhPKAPMQQAcM2vmMLGmz7mmKfKT4jorr2tgEAHm8Gym8W5Z2abNPzQWfdNmrXbravVPjBI2VhXVVBaL1SOshUEW9CpqLjcfd1LzrK8hrt%2F8s3ULcXKWC5Ayqe4yQLNF93KLcD69OPoj9DAoQ69kvlojwr2FdZSaKD9JWaRrtj9rhTxptrNohTN5L1r6V1rBMuXKGxp46wctEm2PXn2cJjRzP3Fv22R%2BuughoRchEc3sCpS3Lr4NKsXVrGES33wqPyu2WbonsxSOp4UxrJw5ua5unxx%2BFgvgv%2BcNNu8oWViPq8zrWVRb8Ar3QjLv2qmclOkli%2BMXxjYrRZRhbjgLw5iYljp41YS1u0CvjS94yhnTKWloF8uCk%2FzUV1Hbi5w9Ih2YPJwEuDTlDcDB52bLwjEDjr3DEZh0vCab1Vf8bMiEwrvbem%2Beg43cFVXHIlhnhHR0jLqumMNzWVNoVC55WsQoa6Z6mVB6tlGAz4VOyeX5FSFSwxz9W35H0ImKtxfZWvtkLGSWw%2BS2qcpt025jlOlO0fbOBUTX0xeyzYHp6XIueiwvTWS9Q53G9s0LQ5c5ip%2BifW5rSKnji2nvH7edswfN1ZEvVpG21%2B4uY6IfZodwyMlI2LZYFeD0mNFU7Rgc2AXCpDi1d1KdU3ZuPEXc9O4cHNAO96%2FzgSHwKTOaG8vyzanfROM40Ufn5nh753RCVjxALOW45PhSBvf8CJrOPS8Pu1oz3%2BxzFhyOFjpg%2BjKFhNWbXZxgXhCxYOIBCI4siPkpTSVdEx2RZWEumI7ATNAXQjm5K6iYNO2ZlBcYM5qA718CyUIjrn0ccLqICHCPX%2FlBFSGOPmAOrBEH7n%2BZ0%2Fr5n3Pce1TTQ8hSeHDWMx%2Fn7zB2R0iH87HxAHMjKsD1GXfSSeNFDngpY1%2Bn5eYecpo9NjfQXNSURmMKIzdSXgDtxWtpDLjueVAAoMmS%2Bqbt%2BTSSpzTcT6oKuxj2JMp1hsYS2B4WWggb5RGK01t%2FT8%2Fg2o%2FwRFcm1CeE9kT1NKVwrdTfwP8ipn41XERhOub7h%2BVRS4jPw6F55rZQfLV%2Fq60pdcD1u%2FG%2F5%2Fzgr5jrLPMjtqE74F7A5%2FdYdBdYW9DAo%2B%2B%2BN8H8cReoX3E5MUxE6A8fr971J58fcA3FQXnEf%2FKIVR1AG%2BD80%2BV8gXxvGbZYrTEHjq%2F9MPevPYI1iWVIMEnPGmChR4bxeUG8ZxdpKOLm3m%2F9RDK7Fq3Z5kGdDKBx5bPWxDIwYWsJpkPqCxeS52C%2BTooNih98zQLLP3%2F3NRET8fL289e9nQK8%2FDnnD19zngEKEOWrSXHcw3Hx1p9Wc8nRONph2y4MjubhFBQ77AIAdkJGLbUo%2BwGUNXdlCUutZNLOWb1ForVXlAIjjEtetCFF7bbtvL%2BRTUhrHOrWW1%2Fc1Weq6uShirbHNRpvBU43AkJxD7VxerMml82p3d8wvrw%2BlK2I0sldyvpoLTil6rvWsRiMdUBMsNNM7oLgoZ9L9FAHuhHoZwt%2FOOSuPSWr1o194pOBwAqSCuzDfDtsSWCfkFaMjSstd9kZzyxzmxd4tbMcNAhu80iROZN5QXyPL4rQES6P8s5tf41I7opspdkcjCC58BlbON7Rczeuwwmxvt%2FP6pHezEM8Im01X03LkuSVMWSlrzGLSkgItD%2Bj6N%2FCqrhCWAXHb7BqAT1DfZthEG7m28Mqnr0J31vg79w3BSt%2F0ED5hqRMY11XmzkbHN7w0dqM8ArV%2BbCC8RukKNU7RU4ehV7Oyv0NnVQWsowN73EYzjBlhDWCZSbw3uGsWdME8%2FANN7P%2BThvR5Z6lA855pskpxGlUY8gzZ%2FOYxvnwFASvRTTpVQtp8vKkQSiCkOpPOtZOkf0f0h3CnQy%2B6V79ne4KTHEs8KgGwiwDfcLANuKnNoYXAIAl29yD53M2VeGYB31ZsxRqbsSyNpcLTpfqfMPiTaOQhDKdM%2FEC8UXAjFyjThXLn0wc71ykYgAxSb5v7HNsh4im6etMLhB3st8W97MsCVMxPdRF0IRtWTP%2B%2BRGZwd6Id5TAYg1xvcp9xhebKg5reLRcVTQzrv5y5i8dd5Oy8t7UD27XrRjDeyq%2F0x3fFSz%2B2t7c%2FFqpuXuJ%2FF7Kd6wCEkb3d%2FGSPmpZayQnkUh95E5Ud0UamjPN69VdGipTvJwyEma%2Fa%2BxqXPZrXMCWvNg1AN%2F4hHFQLcUhFkwOo6wTq%2BbczUGgXkv7oA%2F5paMNe1KDfpE295oYz1fqTtb3arXEo5fhzsYpyUMz1prAhsihU0qvcdxkM8mwxAGmJWVmgC0nCMhJml4ls2CYdBJa4Ah2znHrfjAIsEoDJiJposlH7jztoolPX7w2MCUEJtQESySDPHOLA9Qb3gD%2FikOWFBkvYSAcMxrrTPIEO%2BjJh62QI4GU9wT8O2TBghr9BW4jcby94uCt15WBYwBbRGQFeMudUg1W0e%2FlGP0n0Iu9IIwzvR9i0YXQSJh3n1AGjVvSYwfIrLi0sWS94A8WduJQP%2BHqH%2BDzCZuPV8vs6MyP8OfU9CNmv8KqH0BIPcI%2FK17rufTP%2FwMk2%2B165EoIaST9tRV%2FQ7TnLs%2FW%2By33OxwOIeHQ%2F0bL%2Fm%2FBKDD9v8KojnBq5dwPqLxNSjYuO46wiOZckIn6bNnZUyee5JDgYKvLTg4AMLaAydD%2BRqBhMyiBYzZfWjOyaTZk28Zlx7AmMq%2B64g2jNPfATNrrGB9VbyxQ%2F2gXO8MJVHhUm6LwcpKYLdFr9SF3692lk90m9uYHw1gHzI7ScApSrjmkoDc1VWp8%2FC6aV6PgYeUDTeMlvnKNN5GluO1WJSySCU1xFU5SgOd40G%2BR9rzK%2BhLM4By3IkqyK26gRr3FCFgwb4Ww%2BHl1QkhYARPMLpGHNNiNMAlVSUMXHfZuxgTbgkSvQSDcke01b3Z7tb2V9DaVYiDr2BgCDNf1ut%2BFJnZzBrRRpWw1xdY6p3c%2F3jn%2Bkc7xQXdZjKUAIezj9jYND9xU8Q1BdCh1xBGSSxuBrKn7sLVcmuT%2BNdjj2mdqt3%2BeJnnSx2wLRZjHgdl4jgAbjsj%2FhzLsPE%2BT%2FX%2FqNClxIPh2muSxv%2BkshVe6vk%2BRnDzphYzD6%2FPFH%2F46RRYvGgq7t1kv%2BMUr%2Fv%2FpDjvA%2FOcUgfv8ShFuAs%2FnKmif6cI%2BXJrypHqb4TU7V%2Bz1INCDZJCsYO9zeDTi0BBFjDouMnyvyGeuG462IUl4d1AKtNevrjxcJAaUJF%2FdhN3AGdNgWUfe22Cmug%2BtwFV3xBbvDTWFjSXCl5fUkPGMlAjDXclVeozSPmjdaordS%2Bdhh9POT4V6uU04Yd2nzEf2WLd12HXpUdugdvTFijMeb%2Fc1a%2BATWRkKvUbRxiHQvsNpejnK61r1xCzs1tO9GAVvCBp68M%2F5JMYIU7dqq3l37iYjJblp1UEUk8rew1aS2aDjY92rhuCkx9M0ALFCK6fbywf1liSOn1%2FElmBmfbcbN%2Fo0xkhRV7kAq5C4DqI0M49rcIjPydZXcJ5CyBGk8AQFxMIsoKfA%2FtXx89MTskxBA6%2FT1cS9Ic14Q5rJQXgSNeYVHzGXmh7ELPcoa4axTEjEacnqhBuGnFJSv3LjP7yRcb62Mjw4IwJzPc8Reyh7UvdOFnJrzqpjT5OW1cuo5U5oeA6hIJ0LxtrQiTPfVl5krexpQFFriAKSkcWBa9u9Py7l9po4ni%2FopKGxD8E3iAKTV%2BWAlSV1vefINRTVbIxCgzgwVzNm3RuNUL2hnDa2MzIFv8sjmitKVlnNPbHuNrd1LT11UC8Kgs50cxblWfLjUhji3YPQ6M2O9lVvTrLLjFqhCTF4H4r1Gd9S1%2BPlNh0mZy86Pl%2BfAI6KzN03g51QqlpkRNNmw4WXTgMgJOqD3lnnLSPmKNY1HEoTpisiTYC0oN1TgKuV3RXJWCK553JTZcy8XQ4jQe%2FXRkOKPtgeMXlX25j4EG6H9Gz4Ejd9%2BbvOH%2Fnz3Rry%2FX3bn2%2FiXl8XXh9Bnm%2FEf%2Fw48t8pF1AC&MD=20170203060248092183
```

> Request query example

```json
{
    "type": "backFromPayment",
    "sid": "58d62200-f93c-4ae2-b93e-0b9ee69ff819",
    "jSessionId": "",
    "orderId": "07904d9f-c7d8-4fbd-884f-232ea3e5ff3a",
    "gdsOrderId": "229458605",
    "payId": "f53687b1-c221-4aa2-954a-e41e07128686",
    "success-metaFrom": "23763",
    "success-metaTo": "23364",
    "success-date": "08022017",
    "success-fromName": "Большая Волга",
    "success-toName": "Дубна",
    "successRedirect": "https://www.onetwotrip.com/ru/poezda/ticket/8d3415c4-812a-4f7d-84a1-8a2cda6f2380/",
    "failure-metaFrom": "23763",
    "failure-metaTo": "23364",
    "failure-date": "08022017",
    "failure-fromName": "Большая Волга",
    "failure-toName": "Дубна",
    "failureRedirect": "https://www.onetwotrip.com/ru/poezda/pay/8d3415c4-812a-4f7d-84a1-8a2cda6f2380/",
    "paymentId": "ca3398e1-531f-4edf-b22d-8b5d121a3aff"
}
```

> Request body example

```
PaRes:eJzNWdnOo8iSfpVWz6XVzW6b1l+/lOyLwYBZjO/YzA5msVmeftJ2VXV1nT4zZ87F0ViySILIyIiM5YuEDzvrk4Q7JdG9Tz4/tGQYgjT5JY+//Iqj2A7FUQLdoji5R2kc2xO/fn4YwEqGF4OnmNF2mxy7FOtUc+32tyOvAsjySPohb5tP7Hf0d/wD+XYLpfdRFjTj50cQdYysf5K7LY7uP5Cvtx910svcJ4qiJIVhOImiGIrufvs2+EDezz+QPwUZ9+dogKrPefypFWDWV7BonDbpBVh0Lp2OtjwfOe3LB/Lk+IiDMfn8Ztov6PYPFP+DhCq86B+3pzhQt3coe0ehH8iPhA+4R33SRMvnliQ+kO93H8l8a5sEckBjv48/kD91uwXN06rvP2yHwSWf1A/7/Pkx5vVPOhF/4JDhRf8YxmC8D5/+B/J19BEFj8cnAIABpxIXWVCl9UUUbd8H7x+09cXykUT5J0pBpeD1NQuytn0+ZvVT1b8SPpCnKsjLvZ8fpzxt4GJ98stcV83w5ddsHG9/IMg0Tb9PxO9tnyI4NARBaQQyxEOe/tev71lJLDfX9v80jQ2atsmjoMrXYISRoiVj1sa/fNft78TY1lMShlg8+xsU9VuEkc1vTwpKYBSUify90B8s+1dW+VnZfgh+G7IAey7wk6DPDyu5Js+ISH5xLPnLr//1T/ODy9NkGP+d9b+t/aOEb/LcoLonn0TTFpIknWHQz22BWSh36OI+JYRI/vJt3pvzA/mu8Fdr3q77YYvejBhRHDe7HB8ngDeBLuuPyilqTucZFx1uI+WoJVqpVbzc0Rr38XK3HFiQHYdcLfeNZO7ynXRqb/dLlaqYfUN09hK5bt535gmcqFuDXlrX2h6mS2OuE36zirDcXpDFKYd6W2YgycbelTe7+3lcy+5qI+u1v80ndNMZQO+URL3Tjuj2ix/SV0k0KvnSHhmt29zUbsA3pva4KAmVPh5zYp73WJjYogcexMKH8Roc6etgE7eRR/Zr7yTnblCXIPMdd7XIo3dFg5y7lee6OBJrknp9xyPBxbC4PcpUsrvjHxsSR/mcyrRsZoJcaXb5YaYKX1YLOzwHxcaZTeSyje95wzbYTe/Yu7dj8kWkdpz55csPIfTVI2qyvD1wplCaC8bgPWKTfsyvMJZhidJkWfALlmX4LgWTzIBUNu3mhF0cBbWovGbVA8fj6sFZgc6kZZeVuUhPKAPMQQAcM2vmMLGmz7mmKfKT4jorr2tgEAHm8Gym8W5Z2abNPzQWfdNmrXbravVPjBI2VhXVVBaL1SOshUEW9CpqLjcfd1LzrK8hrt/8s3ULcXKWC5Ayqe4yQLNF93KLcD69OPoj9DAoQ69kvlojwr2FdZSaKD9JWaRrtj9rhTxptrNohTN5L1r6V1rBMuXKGxp46wctEm2PXn2cJjRzP3Fv22R+uughoRchEc3sCpS3Lr4NKsXVrGES33wqPyu2WbonsxSOp4UxrJw5ua5unxx+Fgvgv+cNNu8oWViPq8zrWVRb8Ar3QjLv2qmclOkli+MXxjYrRZRhbjgLw5iYljp41YS1u0CvjS94yhnTKWloF8uCk/zUV1Hbi5w9Ih2YPJwEuDTlDcDB52bLwjEDjr3DEZh0vCab1Vf8bMiEwrvbem+eg43cFVXHIlhnhHR0jLqumMNzWVNoVC55WsQoa6Z6mVB6tlGAz4VOyeX5FSFSwxz9W35H0ImKtxfZWvtkLGSWw+S2qcpt025jlOlO0fbOBUTX0xeyzYHp6XIueiwvTWS9Q53G9s0LQ5c5ip+ifW5rSKnji2nvH7edswfN1ZEvVpG21+4uY6IfZodwyMlI2LZYFeD0mNFU7Rgc2AXCpDi1d1KdU3ZuPEXc9O4cHNAO96/zgSHwKTOaG8vyzanfROM40Ufn5nh753RCVjxALOW45PhSBvf8CJrOPS8Pu1oz3+xzFhyOFjpg+jKFhNWbXZxgXhCxYOIBCI4siPkpTSVdEx2RZWEumI7ATNAXQjm5K6iYNO2ZlBcYM5qA718CyUIjrn0ccLqICHCPX/lBFSGOPmAOrBEH7n+Z0/r5n3Pce1TTQ8hSeHDWMx/n7zB2R0iH87HxAHMjKsD1GXfSSeNFDngpY1+n5eYecpo9NjfQXNSURmMKIzdSXgDtxWtpDLjueVAAoMmS+qbt+TSSpzTcT6oKuxj2JMp1hsYS2B4WWggb5RGK01t/T8/g2o/wRFcm1CeE9kT1NKVwrdTfwP8ipn41XERhOub7h+VRS4jPw6F55rZQfLV/q60pdcD1u/G/5/zgr5jrLPMjtqE74F7A5/dYdBdYW9DAo+++N8H8cReoX3E5MUxE6A8fr971J58fcA3FQXnEf/KIVR1AG+D80+V8gXxvGbZYrTEHjq/9MPevPYI1iWVIMEnPGmChR4bxeUG8ZxdpKOLm3m/9RDK7Fq3Z5kGdDKBx5bPWxDIwYWsJpkPqCxeS52C+TooNih98zQLLP3/3NRET8fL289e9nQK8/DnnD19zngEKEOWrSXHcw3Hx1p9Wc8nRONph2y4MjubhFBQ77AIAdkJGLbUo+wGUNXdlCUutZNLOWb1ForVXlAIjjEtetCFF7bbtvL+RTUhrHOrWW1/c1Weq6uShirbHNRpvBU43AkJxD7VxerMml82p3d8wvrw+lK2I0sldyvpoLTil6rvWsRiMdUBMsNNM7oLgoZ9L9FAHuhHoZwt/OOSuPSWr1o194pOBwAqSCuzDfDtsSWCfkFaMjSstd9kZzyxzmxd4tbMcNAhu80iROZN5QXyPL4rQES6P8s5tf41I7opspdkcjCC58BlbON7Rczeuwwmxvt/P6pHezEM8Im01X03LkuSVMWSlrzGLSkgItD+j6N/CqrhCWAXHb7BqAT1DfZthEG7m28Mqnr0J31vg79w3BSt/0ED5hqRMY11XmzkbHN7w0dqM8ArV+bCC8RukKNU7RU4ehV7Oyv0NnVQWsowN73EYzjBlhDWCZSbw3uGsWdME8/ANN7P+ThvR5Z6lA855pskpxGlUY8gzZ/OYxvnwFASvRTTpVQtp8vKkQSiCkOpPOtZOkf0f0h3CnQy+6V79ne4KTHEs8KgGwiwDfcLANuKnNoYXAIAl29yD53M2VeGYB31ZsxRqbsSyNpcLTpfqfMPiTaOQhDKdM/EC8UXAjFyjThXLn0wc71ykYgAxSb5v7HNsh4im6etMLhB3st8W97MsCVMxPdRF0IRtWTP++RGZwd6Id5TAYg1xvcp9xhebKg5reLRcVTQzrv5y5i8dd5Oy8t7UD27XrRjDeyq/0x3fFSz+2t7c/FqpuXuJ/F7Kd6wCEkb3d/GSPmpZayQnkUh95E5Ud0UamjPN69VdGipTvJwyEma/a+xqXPZrXMCWvNg1AN/4hHFQLcUhFkwOo6wTq+bczUGgXkv7oA/5paMNe1KDfpE295oYz1fqTtb3arXEo5fhzsYpyUMz1prAhsihU0qvcdxkM8mwxAGmJWVmgC0nCMhJml4ls2CYdBJa4Ah2znHrfjAIsEoDJiJposlH7jztoolPX7w2MCUEJtQESySDPHOLA9Qb3gD/ikOWFBkvYSAcMxrrTPIEO+jJh62QI4GU9wT8O2TBghr9BW4jcby94uCt15WBYwBbRGQFeMudUg1W0e/lGP0n0Iu9IIwzvR9i0YXQSJh3n1AGjVvSYwfIrLi0sWS94A8WduJQP+HqH+DzCZuPV8vs6MyP8OfU9CNmv8KqH0BIPcI/K17rufTP/wMk2+165EoIaST9tRV/Q7TnLs/W+y33OxwOIeHQ/0bL/m/BKDD9v8KojnBq5dwPqLxNSjYuO46wiOZckIn6bNnZUyee5JDgYKvLTg4AMLaAydD+RqBhMyiBYzZfWjOyaTZk28Zlx7AmMq+64g2jNPfATNrrGB9VbyxQ/2gXO8MJVHhUm6LwcpKYLdFr9SF3692lk90m9uYHw1gHzI7ScApSrjmkoDc1VWp8/C6aV6PgYeUDTeMlvnKNN5GluO1WJSySCU1xFU5SgOd40G+R9rzK+hLM4By3IkqyK26gRr3FCFgwb4Ww+Hl1QkhYARPMLpGHNNiNMAlVSUMXHfZuxgTbgkSvQSDcke01b3Z7tb2V9DaVYiDr2BgCDNf1ut+FJnZzBrRRpWw1xdY6p3c/3jn+kc7xQXdZjKUAIezj9jYND9xU8Q1BdCh1xBGSSxuBrKn7sLVcmuT+Ndjj2mdqt3+eJnnSx2wLRZjHgdl4jgAbjsj/hzLsPE+T/X/qNClxIPh2muSxv+kshVe6vk+RnDzphYzD6/PFH/46RRYvGgq7t1kv+MUr/v/pDjvA/OcUgfv8ShFuAs/nKmif6cI+XJrypHqb4TU7V+z1INCDZJCsYO9zeDTi0BBFjDouMnyvyGeuG462IUl4d1AKtNevrjxcJAaUJF/dhN3AGdNgWUfe22Cmug+twFV3xBbvDTWFjSXCl5fUkPGMlAjDXclVeozSPmjdaordS+dhh9POT4V6uU04Yd2nzEf2WLd12HXpUdugdvTFijMeb/c1a+ATWRkKvUbRxiHQvsNpejnK61r1xCzs1tO9GAVvCBp68M/5JMYIU7dqq3l37iYjJblp1UEUk8rew1aS2aDjY92rhuCkx9M0ALFCK6fbywf1liSOn1/ElmBmfbcbN/o0xkhRV7kAq5C4DqI0M49rcIjPydZXcJ5CyBGk8AQFxMIsoKfA/tXx89MTskxBA6/T1cS9Ic14Q5rJQXgSNeYVHzGXmh7ELPcoa4axTEjEacnqhBuGnFJSv3LjP7yRcb62Mjw4IwJzPc8Reyh7UvdOFnJrzqpjT5OW1cuo5U5oeA6hIJ0LxtrQiTPfVl5krexpQFFriAKSkcWBa9u9Py7l9po4ni/opKGxD8E3iAKTV+WAlSV1vefINRTVbIxCgzgwVzNm3RuNUL2hnDa2MzIFv8sjmitKVlnNPbHuNrd1LT11UC8Kgs50cxblWfLjUhji3YPQ6M2O9lVvTrLLjFqhCTF4H4r1Gd9S1+PlNh0mZy86Pl+fAI6KzN03g51QqlpkRNNmw4WXTgMgJOqD3lnnLSPmKNY1HEoTpisiTYC0oN1TgKuV3RXJWCK553JTZcy8XQ4jQe/XRkOKPtgeMXlX25j4EG6H9Gz4Ejd9+bvOH/nz3Rry/X3bn2/iXl8XXh9Bnm/Ef/w48t8pF1AC
MD:20170203060248092183
```

> Response

```http
HTTP/1.1 301 Moved Permanently
Server: nginx/1.8.1
Date: Thu, 02 Feb 2017 17:30:43 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive
Location: https://www.onetwotrip.com/ru/poezda/ticket/8d3415c4-812a-4f7d-84a1-8a2cda6f2380/?metaFrom=23763&metaTo=23364&date=08022017&fromName=%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F%20%D0%92%D0%BE%D0%BB%D0%B3%D0%B0&toName=%D0%94%D1%83%D0%B1%D0%BD%D0%B0
Set-Cookie: _sst=1486048642933|1486101844347|e07c619c2845d0236556c8e0097e3a6a; path=/
Set-Cookie: sid=T5oTMwKZVSCFFsroXjCqaxI8; expires=Sat, 4-Feb-2017 06:04:04 GMT; path=/; domain=.onetwotrip.com
```

> Response body without redirect

```json
{
  "status": "accept",
  "paymentId": "ca3398e1-531f-4edf-b22d-8b5d121a3aff",
  "orderId": 229458605
}
```

### HTTP Request

`POST $API_HOST/_api/rzd/orderManager?type=backFromPayment`

### Query String Parameters

Параметр | Описание
--------- | -----------
type | **backFromPayment** обязательный параметр-ключ
sid | session ID
jSessionId |
orderId | ID заказа
gdsOrderId | ID заказа поставщика
payId | pay ID
success-* | Все параметры с данным префиксом добавятся в query successRedirect
successRedirect | URL для редиректа в случае успеха
failure-* | Все параметры с данным префиксом добавятся в query failureRedirect
failureRedirect | URL для редиректа в случае ошибки
paymentId | payment ID

### Response description
Поле | Описание
--------- | -----------
status | Статус оплаты
paymentId | ID платежа
orderId | ID заказа


## validForPay

Проверка валидности карты по BIN.

> Request

```http
GET /_api/rzd/orderManager?type=validForPay&mi=554759 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: www.onetwotrip.com
```

> Response

```http
HTTP/1.1 200 OK
Cache-control: private
Content-Encoding: gzip
Content-Type: application/json;charset=utf-8
Date: Mon, 06 Feb 2017 11:52:46 GMT
Server: nginx/1.11.3
Set-Cookie: _sst=1486381966349|1486381966349|ddf0710f006e84200ba4fcd2418ecb33; path=/; Secure
Set-Cookie: sid=xAYECl5sNf5omMJYYXFovXTg; expires=Tue, 7-Feb-2017 11:52:46 GMT; path=/; domain=.onetwotrip.com; Secure
Set-Cookie: ENVID=production-b|WJhjk; path=/; Secure
Transfer-Encoding: chunked

{
    "result": true,
    "success": true
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/orderManager?type=validForPay&mi=554759`

### Query String Parameters

Параметр | Описание
--------- | -----------
type | **validForPay** обязательный параметр-ключ
mi | первые 6 цифры карты

### Response description

Параметр | Описание
--------- | -----------
success | пришел ли к успеху
result | валидна ли карта

Необходимо завязываться на `.result` поле в ответе для проверки валидности карты.

<aside class="notice">
	Для тестовой оплаты/оплаты полностью бонусами не нужно проверять валидность карты, т.к. карта тестовая карта 411111111111 не валидна.
</aside>

## payment

<aside class="warning">
    Deprecated - полностью повторяет функционал createOrder
</aside>

### HTTP Request

`POST $API_HOST/_api/rzd/payment`

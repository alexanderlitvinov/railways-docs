# searchManager

## timetable

Получение списка поездов по выбранному маршруту на дату.

<aside class="notice">
	Формат ответа полностью совпадает с `/rzd/metaTimetable`. Отличие в том, что `timetable` ищет только по заданым станциям, когда `metaTimetable` запускает поиск по всем станциям внутри мета-станции (прямое произведение всех станций внутри меты).
</aside>

> Request

```http
GET /_api/rzd/searchManager?date=29012017&from=2000000&to=2004000&type=timetable&device_id=0b05988b-7714-4720-8c1f-0a8d6f6e437e-e7044e7e63a40aca8547e25119c361fe&device_name=google+Nexus+7&lat=55.7782922&long=37.6241382 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: www.onetwotrip.com

```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Wed, 25 Jan 2017 07:22:43 GMT
Set-Cookie: _sst=1485328961416|1485328961416|d12ec764335e2fa019d8d9e250c265c1; path=/
Set-Cookie: sid=LdbRN1o6p1AuRbfTBGKJZd4h; expires=Thu, 26-Jan-2017 07:22:41 GMT; path=/
Set-Cookie: sid=LdbRN1o6p1AuRbfTBGKJZd4h; expires=Thu, 26-Jan-2017 07:22:41 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "result": [
        {
            "arrival": {
                "local": false,
                "time": "2017-01-29T08:59:00"
            },
            "class": [
                "скорый"
            ],
            "departure": {
                "local": false,
                "time": "2017-01-29T00:20:00"
            },
            "durationInMinutes": 519,
            "elReg": true,
            "from": {
                "code": 2000000,
                "station": "Москва"
            },
            "name": "МЕГАПОЛИС",
            "places": [
                {
                    "commissions": [
                        {
                            "amount": 0,
                            "cost": 1,
                            "currency": "RUB",
                            "type": "percent_markup"
                        }
                    ],
                    "cost": 1484.2,
                    "freeSeats": 184,
                    "gdsCost": 1483.2,
                    "type": 4,
                    "typeName": "Купе"
                },
                {
                    "commissions": [
                        {
                            "amount": 0,
                            "cost": 1,
                            "currency": "RUB",
                            "type": "percent_markup"
                        }
                    ],
                    "cost": 3557.6,
                    "freeSeats": 17,
                    "gdsCost": 3556.6,
                    "type": 6,
                    "typeName": "Люкс"
                }
            ],
            "route": [
                "Москва октябрьская",
                "Санкт-Петербург-Главн."
            ],
            "to": {
                "code": 2004000,
                "station": "Санкт-Петербург"
            },
            "trainNumber": "020У"
        }
    ],
    "success": true
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/searchManager?date=29012017&from=2000000&to=2004000&type=timetable&device_id=0b05988b-7714-4720-8c1f-0a8d6f6e437e-e7044e7e63a40aca8547e25119c361fe&device_name=google+Nexus+7&lat=55.7782922&long=37.6241382`

### Required params

Параметр | Описание
--------- | -----------
type | обязательный параметр-ключ timetable
date | дата отправления в формате DDMMYYYY
from | код станции отправления
to | код станции прибытия
device_id | идентификатор девайса
device_name | название девайса
lat | широта
long | долгота

### Debug params

Параметр | Default | Описание
--------- | ---- | ---------
origin | true | посмотреть ответ от поставщика

### Response description
Поле | Описание
--------- | -----------
trainNumber | номер поезда по прибытию
trainNumber2 | номер поезда по отправлению
durationInMinutes |время в пути
class  | класс поезда
name |название поезда (не обязательно)
timeInWay | минут в пути
route  | маршрут движения поезда (может быть > 2 значений)
from  | откуда еду
to  | куда приезжаю
departure.local |признак локального времени
arrival.local |признак локального времени
places  | свободные места
places.typeName | название типа вагона
places.freeSeats | свободно мест
places.cost | минимальная цена в рублях за место в этом типе с наценкой и бельем
places.tariffService | Цена белья если можно купить без него
places.type | тип
places.gdsCost | цена без наценки
elReg | Наличие электронной регистрации

Если в ответе есть trainNumber и trainNumber2, то пользователю нужно выводить trainNumber2 т.к. этот тот номер поезда который нужно искать на табло когда уезжаешь.

## trainCarPlaces

Получение информации о выбранном поезде по маршруту на выбранную дату и типу вагонов (вагоны, тарифы).

> Request

```http
GET /_api/rzd/searchManager?type=trainCarPlaces&carType=4&date=29012017&from=2000000&to=2004000&train=004%D0%90&child=true HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: onetwotrip.com
```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Wed, 25 Jan 2017 10:36:50 GMT
Set-Cookie: _sst=1485340608201|1485340608201|2afd13dbc411af51e7d26ba88f50581e; path=/
Set-Cookie: sid=W1NkfYm6Qt2s5z/qUl9nfgR3; expires=Thu, 26-Jan-2017 10:36:48 GMT; path=/
Set-Cookie: sid=W1NkfYm6Qt2s5z/qUl9nfgR3; expires=Thu, 26-Jan-2017 10:36:48 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "result": {
        "cars": [
            {
                "carrier": "ФПК",
                "classService": {
                    "id": "2Э",
                    "name": "Купейный вагон повышенной комфортности с УКВ и ЭЧТК."
                },
                "clsType": "2Э",
                "elReg": true,
                "includesBedding": true,
                "number": 9,
                "signs": [
                    "У1"
                ],
                "tariffs": [
                    {
                        "ages": {
                            "child": {
                                "age": 9,
                                "max": 4
                            },
                            "infant": {
                                "age": 4,
                                "max": 2
                            }
                        },
                        "clsType": "2Э",
                        "commissions": [
                            {
                                "amount": 0,
                                "cost": 1,
                                "currency": "RUB",
                                "type": "percent_markup"
                            }
                        ],
                        "gdsPrice": 1153.4,
                        "includesBedding": true,
                        "places": [
                            8,
                            30,
                            36
                        ],
                        "price": 1154.4,
                        "priceService": 698,
                        "seats": {
                            "SeatsUndef": "3"
                        },
                        "selectBedding": false,
                        "signs": [
                            "У1"
                        ]
                    },
                    {
                        "ages": {
                            "child": {
                                "age": 9,
                                "max": 4
                            },
                            "infant": {
                                "age": 4,
                                "max": 2
                            }
                        },
                        "clsType": "2Э",
                        "commissions": [
                            {
                                "amount": 0,
                                "cost": 1,
                                "currency": "RUB",
                                "type": "percent_markup"
                            }
                        ],
                        "gdsPrice": 1608.7,
                        "includesBedding": true,
                        "places": [
                            5,
                            7,
                            9,
                            11,
                            23,
                            25,
                            27,
                            29,
                            31,
                            33,
                            35
                        ],
                        "price": 1609.7,
                        "priceService": 698,
                        "seats": {
                            "SeatsUndef": "11"
                        },
                        "selectBedding": false,
                        "signs": [
                            "У1"
                        ]
                    }
                ],
                "trainLetter": "А"
            }
        ],
        "train": {
            "arrival": {
                "code": "2004000",
                "country": {
                    "code": 20,
                    "name": {
                        "en": "Russia",
                        "ru": "Россия"
                    }
                },
                "local": false,
                "time": "2017-01-30T08:30:00"
            },
            "carTypes": [
                {
                    "commissions": [
                        {
                            "amount": 0,
                            "cost": 1,
                            "currency": "RUB",
                            "type": "percent_markup"
                        }
                    ],
                    "cost": 2000,
                    "freeSeats": 168,
                    "gdsCost": 1999,
                    "type": 4,
                    "typeName": "Купе"
                },
                {
                    "commissions": [
                        {
                            "amount": 0,
                            "cost": 1,
                            "currency": "RUB",
                            "type": "percent_markup"
                        }
                    ],
                    "cost": 11658.8,
                    "freeSeats": 8,
                    "gdsCost": 11657.8,
                    "type": 5,
                    "typeName": "Мягкий"
                },
                {
                    "commissions": [
                        {
                            "amount": 0,
                            "cost": 1,
                            "currency": "RUB",
                            "type": "percent_markup"
                        }
                    ],
                    "cost": 4000,
                    "freeSeats": 60,
                    "gdsCost": 3999,
                    "type": 6,
                    "typeName": "Люкс"
                }
            ],
            "class": [
                "скорый",
                "фирменный"
            ],
            "departure": {
                "code": "2000000",
                "country": {
                    "code": 20,
                    "name": {
                        "en": "Russia",
                        "ru": "Россия"
                    }
                },
                "local": false,
                "time": "2017-01-29T23:30:00"
            },
            "durationInMinutes": 540,
            "name": "Экспресс",
            "number": "004А",
            "route": [
                "Москва октябрьская",
                "Санкт-Петербург-Главн."
            ]
        }
    },
    "success": true
}
```

### Request params
Поле | Описание
--------- | -----------
type | **trainCarPlaces** обязательный параметр-ключ
carType | категория вагонов
date | дата отправления в формате DDMMYYYY
from | код станции отправления
to | код станции прибытия
train | номер поезда
child | если нужно получить детский тариф
origin | true - посмотреть ответ от поставщика

### Response description
Поле | Описание
--------- | -----------
train.durationInMinutes 540 |время в пути
train.route  |может быть больше 2
train.departure | время отправления
train.departure.time | дата и время
train.departure.stopInMinutes |Не обязательно
train.departure.local | признак местного времени
train.arrival | время прибытия
train.arrival.time | дата и время
train.arrival.stopInMinutes |Не обязательно
train.arrival.local | признак местного времени
train.arrival.name | Название поезда или бренд (не обязательно)
carTypes | список типов вагонов в поезде
carTypes.typeName |текстовое представление типа вагона
carTypes.freeSeats |кол-во свободных мест в вагоне
carTypes.cost | минимальная цена места в вагоне с наценкой
carTypes.type |типа вагон
carTypes.gdsCost | цена без наценки
cars | список вагонов
cars.number | номер вагона
cars.classService | объект с текстовым описанием класса
cars.name Вагон | пояснение к классу обслуживания
cars.id | класс обслуживания
cars.clsType | тип класса обслуживания
cars.carrier | перевозчик
cars.signs  | признаки вагона
cars.tariffs | список тарифов
cars.tariffs.seats  | детализация мест
cars.tariffs.SeatsUndef | места без указания верх/нижн
cars.tariffs.SeatsDn | нижние
cars.tariffs.SetasUp | верхние
cars.tariffs.SeatsLateralDn | боковые нижние
cars.tariffs.SeatsLateralUp | боковые верхние
cars.tariffs.FreeComp | целые купе
cars.tariffs.price | общий тариф (вкл билет плацкарту сервис наценку)
cars.tariffs.gdsPrice | цена без наценки
cars.tariffs.priceService | стоимость сервиса (белья для плацкарта)
cars.tariffs.selectBedding | можно ли выбирать с бельем или без белья
cars.tariffs.includesBedding | включает ли цена gdsPrice и price в себя цену белья. (меняется из bo)
cars.tariffs.places | номера свободных мест допускаются числа и кирилические символы Ц - целое купе Ж - женское М - мужское С - смешанное
cars.tariffs.saleOnTwo | купе целиком продаются сразу 2 места
cars.tariffs.saleOnFour | купе целиком продаются сразу 4 места
cars.tariffs.ages | возрастные границы детских тарифов
cars.tariffs.ages.infant  | ребенок без места
cars.tariffs.ages.infant.age | полных лет на дату поездки (не более)
cars.tariffs.ages.infant.max | максимум мест по данному тарифу в заказе
cars.tariffs.ages.child | ребенок, но с местом
cars.tariffs.ages.child.age | полных лет на дату поездки (не более)
cars.tariffs.ages.child.max | максимум мест по данному тарифу в заказе


## trainPlaces

Получение информации о выбранном поезде по маршруту на выбранную дату **по всем типам вагонов**.

> Request

```http
GET /_api/rzd/searchManager?type=trainPlaces&date=29012017&from=2000000&to=2004000&train=004%D0%90 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: onetwotrip.com

```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Wed, 25 Jan 2017 10:10:04 GMT
Set-Cookie: _sst=1485339003233|1485339003233|99522e28d4462a471330203b9190b64e; path=/
Set-Cookie: sid=7TDOafSWK3ZZrGEn+t/RPEHa; expires=Thu, 26-Jan-2017 10:10:03 GMT; path=/
Set-Cookie: sid=7TDOafSWK3ZZrGEn+t/RPEHa; expires=Thu, 26-Jan-2017 10:10:03 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "result": {
        "cars": [
            {
                "carType": 6,
                "carrier": "ФПК",
                "classService": {
                    "id": "1Б",
                    "name": "Класс \"Бизнес\". Вагон СВ повышенной комфортности c УКВ и ЭЧТК."
                },
                "elReg": true,
                "includesBedding": true,
                "number": 3,
                "saleOnFour": false,
                "saleOnTwo": false,
                "signs": [
                    "У1"
                ],
                "tariffs": [
                    {
                        "ages": {
                            "child": {
                                "age": 9,
                                "max": 4
                            },
                            "infant": {
                                "age": 4,
                                "max": 2
                            }
                        },
                        "commissions": [
                            {
                                "amount": 0,
                                "cost": 1,
                                "currency": "RUB",
                                "type": "percent_markup"
                            }
                        ],
                        "gdsPrice": 4938.8,
                        "includesBedding": true,
                        "places": [
                            1,
                            3,
                            5,
                            11,
                            13,
                            17
                        ],
                        "price": 4939.8,
                        "priceService": 1337,
                        "seats": {
                            "SeatsUndef": "6"
                        },
                        "selectBedding": false
                    }
                ]
            },
            {
                "carType": 4,
                "carrier": "ФПК",
                "classService": {
                    "id": "2Э",
                    "name": "Купейный вагон повышенной комфортности с УКВ и ЭЧТК."
                },
                "elReg": true,
                "includesBedding": true,
                "number": 11,
                "saleOnFour": false,
                "saleOnTwo": false,
                "signs": [
                    "МЖ",
                    "У1"
                ],
                "tariffs": [
                    {
                        "ages": {
                            "child": {
                                "age": 9,
                                "max": 4
                            },
                            "infant": {
                                "age": 4,
                                "max": 2
                            }
                        },
                        "commissions": [
                            {
                                "amount": 0,
                                "cost": 1,
                                "currency": "RUB",
                                "type": "percent_markup"
                            }
                        ],
                        "gdsPrice": 1999,
                        "includesBedding": true,
                        "places": [
                            "10М",
                            "12М",
                            "30Ц",
                            "32Ц"
                        ],
                        "price": 2000,
                        "priceService": 698,
                        "seats": {
                            "SeatsUndef": "4"
                        },
                        "selectBedding": false
                    },
                    {
                        "ages": {
                            "child": {
                                "age": 9,
                                "max": 4
                            },
                            "infant": {
                                "age": 4,
                                "max": 2
                            }
                        },
                        "commissions": [
                            {
                                "amount": 0,
                                "cost": 1,
                                "currency": "RUB",
                                "type": "percent_markup"
                            }
                        ],
                        "gdsPrice": 1999,
                        "includesBedding": true,
                        "places": [
                            "34Ц",
                            "36Ц"
                        ],
                        "price": 2000,
                        "priceService": 698,
                        "seats": {
                            "SeatsUndef": "2"
                        },
                        "selectBedding": false
                    },
                    {
                        "ages": {
                            "child": {
                                "age": 9,
                                "max": 4
                            },
                            "infant": {
                                "age": 4,
                                "max": 2
                            }
                        },
                        "commissions": [
                            {
                                "amount": 0,
                                "cost": 1,
                                "currency": "RUB",
                                "type": "percent_markup"
                            }
                        ],
                        "gdsPrice": 3300,
                        "includesBedding": true,
                        "places": [
                            "3Ж",
                            "7Ж",
                            "9М",
                            "13Ж",
                            "15Ж",
                            "17Ж",
                            "19Ж",
                            "25Ж",
                            "27Ж",
                            "29Ц",
                            "31Ц"
                        ],
                        "price": 3301,
                        "priceService": 698,
                        "seats": {
                            "SeatsUndef": "11"
                        },
                        "selectBedding": false
                    },
                    {
                        "ages": {
                            "child": {
                                "age": 9,
                                "max": 4
                            },
                            "infant": {
                                "age": 4,
                                "max": 2
                            }
                        },
                        "commissions": [
                            {
                                "amount": 0,
                                "cost": 1,
                                "currency": "RUB",
                                "type": "percent_markup"
                            }
                        ],
                        "gdsPrice": 3300,
                        "includesBedding": true,
                        "places": [
                            "33Ц",
                            "35Ц"
                        ],
                        "price": 3301,
                        "priceService": 698,
                        "seats": {
                            "SeatsUndef": "2"
                        },
                        "selectBedding": false
                    }
                ]
            }
        ],
        "train": {
            "arrival": {
                "local": false,
                "time": "2017-01-30T08:30:00"
            },
            "class": [
                "скорый",
                "фирменный"
            ],
            "departure": {
                "local": false,
                "time": "2017-01-29T23:30:00"
            },
            "durationInMinutes": 540,
            "name": "Экспресс",
            "number": "004А",
            "route": [
                "Москва октябрьская",
                "Санкт-Петербург-Главн."
            ]
        }
    },
    "success": true
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/searchManager?type=trainPlaces&date=29012017&from=2000000&to=2004000&train=004%D0%90`

### Params

Параметр | Описание
--------- | -----------
type | **trainPlaces** обязательный параметр-ключ
date | дата отправления в формате DDMMYYYY
from | код станции отправления
to | код станции прибытия
train | номер поезда

### Response description

Ответ похож на `trainCarPlaces` за исключением того, что на выходы все типы вагонов.

## scheduleStation

Получение информации о проходящих поездах через станцию

> Request

```http
GET /_api/rzd/scheduleStation?date=21022017&amp;station=2004000 HTTP/1.1
Host: www.onetwotrip.com
Cache-Control: no-cache
```

> Response

```http
HTTP/1.1 200 OK
Server: nginx/1.11.3
Date: Wed, 25 Jan 2017 14:55:34 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Set-Cookie: _sst=1485356129395|1485356129395|f9fe93fe1005e1409d5011873ca24270; path=/; Secure
Set-Cookie: sid=zzaCLl26e0855vxZekiydxRq; expires=Thu, 26-Jan-2017 14:55:29 GMT; path=/; domain=.onetwotrip.com; Secure
Content-Encoding: gzip
Set-Cookie: ENVID=production-a|WIi8a; path=/; Secure
Cache-control: private

{
    "success": true,
    "result": {
        "station": "Санкт-Петербург",
        "date": "2017-02-21",
        "depTrains": [
            {
                "number": "119А",
                "daysOfGo": {
                    "type": "dates",
                    "dates": [
                        "21.02",
                        "05.03"
                    ]
                },
                "route": [
                    "Санкт-Петербург-Главн.",
                    "Белгород"
                ],
                "stations": [
                    {
                        "code": 2004001,
                        "station": "Санкт-Петербург-Главн."
                    },
                    {
                        "code": 2014370,
                        "station": "Белгород"
                    }
                ],
                "depTime": "00:11",
                "arvTime": "19:44",
                "timeInWay": "19:33"
            }
        ],
        "arvTrains": [
            {
                "number": "224Ь",
                "route": [
                    "Москва октябрьская",
                    "Санкт-Петербург-Главн."
                ],
                "stations": [
                    {
                        "code": 2006004,
                        "station": "Москва октябрьская"
                    },
                    {
                        "code": 2004001,
                        "station": "Санкт-Петербург-Главн."
                    }
                ],
                "depTime": "17:50",
                "arvTime": "01:21",
                "timeInWay": "07:31"
            }
        ],
        "transitTrains": [
            {
                "number": "034Х",
                "route": [
                    "Таллинн пасс",
                    "Москва октябрьская"
                ],
                "stations": [
                    {
                        "code": 2600001,
                        "station": "Таллинн пасс"
                    },
                    {
                        "code": 2006004,
                        "station": "Москва октябрьская"
                    }
                ],
                "stopTime": "71",
                "depTime": "01:05",
                "arvTime": "09:40",
                "timeInWay": "08:35"
            }
        ]
    }
}
```

> daysOfGo Даты
```json
{
    "type": "dates",
    "dates": [
        "23.02",
        "26.02",
        "04.03"
    ]
}
```

> daysOfGo Одна дата

```json
{
    "type": "all",
    "from": "27.02",
    "to": "27.02"
}
```

> daysOfGo С 10.02 по 14.02 по четным

```json
{
    "type": "even",
    "from": "10.02",
    "to": "14.02"
}
```

> daysOfGo по четвергам и воскресеньям

```json
{
    "type": "weekDays",
    "days": [
        "4",
        "7"
    ]
}
```

> daysOfGo по нечетным

```json
{
    "type": "odd"
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/scheduleStation?date=21022017&station=2004000`

### Params

Параметр | Описание
-------- | -------
station | Код станции
date | Дата в формате DDMMYYYY

### Response description

Поле | Описание
--------- | -----------
station | Название станции
date | Дата в формате YYYY-MM-DD
depTrains | Массив поездов (train) отправляющихся из данной станции
arvTrains | Массив поездов (train) прибывающих на эту станцию
transitTrains | Массив поездов (train) проходящих станцию транзитом

### Train object description
Поле | Описание
--------- | -----------
number | Номер поезда
daysOfGo | Объект с описанием дней следования
route[] | Название станции отправления/назначения
stations[].code | Код станции отправления/назначения
stations[].station | Название станции отправления/назначения
stopTime | Время остановки
depTime | Время отправления HH:mm
arvTime | Вреия прибытия HH:mm
timeInWay | Время в пути HH:mm

### DaysOfGo object description
Поле | Описание
--------- | -----------
type | Тип даты (even/odd/all/weekDays/dates)
dates | Массив конкретных дат DD.MM (при type=dates)
days | Массов дней недели (1-7) (при type=weekDays)
from | С даты DD.MM (опционально)
to | По дату DD.MM (опционально)

## trainsStation

Получение информации о проходящих поездах в заданную дату через станцию

> Request

```http
GET /_api/rzd/searchManager?type=trainsStation&date=21022017&station=2010270 HTTP/1.1
Host: www.onetwotrip.com
Connection: keep-alive
Pragma: no-cache
Cache-Control: no-cache
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36
Accept: */*
Accept-Encoding: gzip, deflate, sdch
Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4
```

> Response

```http
HTTP/1.1 200 OK
Server: nginx/1.11.3
Date: Wed, 25 Jan 2017 15:35:30 GMT
Content-Type: application/json;charset=utf-8
Transfer-Encoding: chunked
Set-Cookie: _sst=1485358529673|1485358529673|8e7a047b4f08af4f68365ad695f85f96; path=/; Secure
Set-Cookie: sid=eQq2IHEtbq81da82QUAgF+D2; expires=Thu, 26-Jan-2017 15:35:29 GMT; path=/; domain=.onetwotrip.com; Secure
Content-Encoding: gzip
Set-Cookie: ENVID=production-a|WIjFx; path=/; Secure
Cache-control: private

{
    "success": true,
    "result": [
        {
            "number": "077Я",
            "route": [
                "ВОРКУТА",
                "С-ПЕТ-ЛАД"
            ],
            "stopTime": "22",
            "depTime": "09:30",
            "arvTime": "18:45",
            "inWay": "33:15",
            "daysOfGo": "ДАТЫ:21.02,24.02,27.02"
        }
    ]
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/searchManager?type=trainsStation&date=28082014&station=2010270`

### Params

Параметр | Описание
--------- | -----------
type | **trainsStation** Обязательный параметр-ключ
date | Дата в формате DDMMYYYY
station | Код станции

### Response description

Поле | Описание
--------- | -----------
number | Номер поезда
route[] | Название станции отправления/назначения
stopTime | Время остановки
depTime | Время отправления HH:mm
arvTime | Вреия прибытия HH:mm
inWay | Время в пути HH:mm
daysOfGo | ЧЕТ/НЕЧЕТ/ДАТЫ:

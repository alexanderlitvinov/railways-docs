---
title: Railways API

toc_footers:
  - <a href='#'>Railways Docs</a>
  - <a href='https://onetwotrip.com/ru/poezda' target="_blank">OneTwoTrip Railways</a>

includes:
  - search_manager
  - schemes
  - book_manager
  - ticket_manager
  - payment
  - statuses
  - errors

search: true
---

# Inroduction

Документация для Railways API. Продажа жд билетов на сайте [OneTwoTrip](https://onetwotrip.com).

# suggestStations

Поиск метастанций по части названия или гео координатам.

> Search by text request

```http
GET /rzd/suggestStations/?searchText=казан&lang=ru&limit=10&flat=true HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: onetwotrip.com
```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Tue, 24 Jan 2017 15:42:41 GMT
Set-Cookie: _sst=1485272561278|1485272561278|c3fb767828431c6a689e495393708445; path=/
Set-Cookie: sid=QXZrNMX0Gk5zbD6Zz6j/R90c; expires=Wed, 25-Jan-2017 15:42:41 GMT; path=/
Set-Cookie: sid=QXZrNMX0Gk5zbD6Zz6j/R90c; expires=Wed, 25-Jan-2017 15:42:41 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "result": [
        {
            "country": "RU",
            "countryId": 20,
            "id": 24705,
            "name": "Казань"
        },
        {
            "country": "KZ",
            "countryId": 27,
            "id": 24701,
            "name": "Казанбасы"
        },
        {
            "country": "UA",
            "countryId": 22,
            "id": 24702,
            "name": "Казанка"
        },
        {
            "country": "RU",
            "countryId": 20,
            "id": 24703,
            "name": "Казаново"
        },
        {
            "country": "RU",
            "countryId": 20,
            "id": 24704,
            "name": "Казановская"
        }
    ],
    "success": true
}
```

> Search by location request

```http
GET /rzd/suggestStations?lon=57.801476&lat=43.704864&rad=50000&limit=10 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: localhost:8080
User-Agent: HTTPie/0.9.2
```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Tue, 24 Jan 2017 15:48:39 GMT
Set-Cookie: _sst=1485272919420|1485272919420|db07751c4db5e7f2f5b811e7e7c0dd47; path=/
Set-Cookie: sid=FGtHHhiYZOyHOYvvjTy8cacg; expires=Wed, 25-Jan-2017 15:48:39 GMT; path=/
Set-Cookie: sid=FGtHHhiYZOyHOYvvjTy8cacg; expires=Wed, 25-Jan-2017 15:48:39 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "result": [
        {
            "country": "UZ",
            "countryId": 29,
            "id": 25168,
            "name": {
                "ru": "Куаныш"
            }
        },
        {
            "country": "UZ",
            "countryId": 29,
            "id": 22918,
            "name": {
                "ru": "Ажинияз"
            }
        },
        {
            "country": "UZ",
            "countryId": 29,
            "id": 23937,
            "name": {
                "ru": "Жаслык"
            }
        },
        {
            "country": "UZ",
            "countryId": 29,
            "id": 23622,
            "name": {
                "ru": "Барса-Кельмес"
            }
        }
    ],
    "success": true
}
```

### HTTP Request

`GET $API_HOST/_api/rzd/suggestStations/?searchText={String}&lang=ru&limit={Number}&flat={Boolean}`

### Search required params

Параметр | Описание
--------- | -----------
searchText | текст запроса (минимум два символа). Раcпознавание раскладки (если вводят название на русском в раскладке en и распознавание языка ввода (если вводять в локали RU название на англ)

### Search not required params

Параметр | Описание
--------- | -----------
lang | язык (локаль) на котором нужно вернуть результаты (значение по умолчанию ru), несколько языков передаются через ',', например ru,en
limit | ограничение по количеству метастанций в ответе (значение по умолчанию 10)
active | если true выводить только активные метастанции, если false все (значение по умолчанию true)
flat | если true и задан только один язык выводить name строкой а не объектом

### Search by geo-location

Параметр | Описание
--------- | -----------
limit | ограничение по количеству метастанций в ответе (значение по умолчанию 10)
active | если true выводить только активные метастанции, если false все (значение по умолчанию true)
lon | долгота местоположения пользователя
lat | широта местоположения пользователя
radius | может использоваться вместе с lon и lat, по умолчанию 50000 м


<aside class="notice">
	Если не найден язык будет использоваться en, но для uk - ru.
</aside>


# metaTimetable

Получение расписания поездов по метастанциям на дату.

> Request

```http
GET /rzd/metaTimetable/?from=22823&to=22871&date=29012017&source=web&long=37.6235121&lat=55.7788674&device_id=nJAQQAlPNaXvhy1wo%2BLxsN3v&device_name=web HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
```

> Response

```http
HTTP/1.1 200 OK
Connection: close
Content-Type: application/json;charset=utf-8
Date: Wed, 25 Jan 2017 07:38:06 GMT
Set-Cookie: _sst=1485329884652|1485329884652|ed9e3ca2277eaaadbd96b60e04817a67; path=/
Set-Cookie: sid=5wEAHpYnlGWVuUdRCDfefN4R; expires=Thu, 26-Jan-2017 07:38:04 GMT; path=/
Set-Cookie: sid=5wEAHpYnlGWVuUdRCDfefN4R; expires=Thu, 26-Jan-2017 07:38:04 GMT; path=/; domain=.onetwotrip.com
Transfer-Encoding: chunked

{
    "result": [
        {
            "arrival": {
                "local": false,
                "time": "2017-01-29T08:59:00"
            },
            "class": [
                "скорый"
            ],
            "departure": {
                "local": false,
                "time": "2017-01-29T00:20:00"
            },
            "durationInMinutes": 519,
            "elReg": true,
            "from": {
                "code": 2000000,
                "station": "Москва"
            },
            "name": "МЕГАПОЛИС",
            "places": [
                {
                    "commissions": [
                        {
                            "amount": 0,
                            "cost": 1,
                            "currency": "RUB",
                            "type": "percent_markup"
                        }
                    ],
                    "cost": 1484.2,
                    "freeSeats": 184,
                    "gdsCost": 1483.2,
                    "type": 4,
                    "typeName": "Купе"
                },
                {
                    "commissions": [
                        {
                            "amount": 0,
                            "cost": 1,
                            "currency": "RUB",
                            "type": "percent_markup"
                        }
                    ],
                    "cost": 3557.6,
                    "freeSeats": 17,
                    "gdsCost": 3556.6,
                    "type": 6,
                    "typeName": "Люкс"
                }
            ],
            "route": [
                "Москва октябрьская",
                "Санкт-Петербург-Главн."
            ],
            "to": {
                "code": 2004000,
                "station": "Санкт-Петербург"
            },
            "trainNumber": "020У"
        }
    ],
    "success": true
}
```

Мета-станция - это сущность объединяющая в себе 1 или более ЖД-станцию.

У мета-станций свой ID (id из ответа `/suggestStations`) по которому и происходит поиск в `/metaTimeTable`.

### HTTP Request

`GET $API_HOST/_api/rzd/metaTimetable/?from=22823&to=22871&date=29012017&source=web&long=37.6235121&lat=55.7788674&device_id=nJAQQAlPNaXvhy1wo%2BLxsN3v&device_name=web`

### Required params

Параметр | Описание
--------- | -----------
type | обязательный параметр-ключ timetable
date | дата отправления в формате DDMMYYYY
from | код станции отправления
to | код станции прибытия
device_id | идентификатор девайса
device_name | название девайса
lat | широта
long | долгота

### Response description
Поле | Описание
--------- | -----------
trainNumber | номер поезда
durationInMinutes |время в пути
class  | класс поезда
name |название поезда (не обязательно)
timeInWay | минут в пути
route  | маршрут движения поезда (может быть > 2 значений)
from  | откуда еду
to  | куда приезжаю
departure.local |признак локального времени
arrival.local |признак локального времени
places  | свободные места
places.typeName | название типа вагона
places.freeSeats | свободно мест
places.cost | минимальная цена в рублях за место в этом типе с наценкой и бельем
places.tariffService | Цена белья если можно купить без него
places.type | тип
places.gdsCost | цена без наценки
elReg | Наличие электронной регистрации

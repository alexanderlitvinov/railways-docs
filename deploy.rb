#!/usr/bin/env ruby

require_relative 'scripts/s3-uploader'

bucket = 'railways-docs'
folder = 'build'

uploader = S3Upload.new(folder, bucket)
uploader.upload!(5, false, true)

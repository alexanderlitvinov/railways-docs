#!/bin/sh
#
# Add commit-msg hook which prepends branchname to all commit messages
#
hookversion=`cat .githooks/version`
installedversion=`cat .git/hooks/version`

export VERSION=$hookversion
if [ -z $installedversion ] || [ $installedversion -lt $hookversion ] ; then
	echo "adding commit-msg hook $hookversion"
	cp .githooks/* .git/hooks/
fi
